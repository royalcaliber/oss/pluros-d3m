Pluros Accelerated Machine Learning Primitives for DARPA D3M
============================================================

[Pluros](https://pluros.io)<sup>&reg;</sup> is a high performance computing
service that lets simple high-level programs access massively parallel hardware.
Pluros acceleration works by offloading compute-intensive function calls to
remote systems, where they can be run on specialized devices or across multiple
machines.

[D3M](https://datadrivendiscovery.org) (Data-driven Discovery of Models) is a
DARPA R&D effort for automated machine learning. The effort consists of three
technical areas: machine learning primitives (TA1), automated pipeline synthesis
and tuning (TA2) and user-in-the-loop auto ML (TA3).

Pluros makes D3M functions available under the
[`ml.d3m`](https://pluros.io/docs/pluros/functions/ml.html) namespace. Please
see the [detailed writeup and demo](https://pluros.io/docs/pluros/topics/d3m)
for more information. This package (`plurosd3m`) contains client-side
compatibility wrappers to access the Pluros functions using the D3M TA1-TA2
interface.


Known Issues
------------

The primitive wrappers in this package are not completely conformant with the
TA1 interface specification, especially with respect to the subclasses
advertised by the proxy primitive. This will be addressed in a future release.


Installation
------------

You can install using pip from gitlab, or from a tarball. It is
highly recommended you use a virtual environment.

```sh
$ python -m venv pluros
$ source pluros/bin/activate
(pluros) $ pip install pluros-d3m
```

If you have not previously installed and setup the Pluros Python client, then
this installation will use the default guest user credentials to access Royal
Caliber's Pluros service at `api.pluros.io`. If that is not what you want, edit
(or create) the Pluros configuration file (at `$HOME/.pluros/pluros.conf` on
unix-y systems) and modify the `[client]` section appropriately.

```ini
[ client ]
   serviceURL = https://api.pluros.io
   user = guest
   apiKey = <relative or absolute path to your api key>
```

Please see the [Pluros documentation](https://pluros.io/docs/client/general)
for further information on how to configure the client and setup credentials.

You can quickly test your setup with:

```
$ python -m plurosd3m --test
```


User Guide
----------

### The Simplified Interface

Pluros provides a simple functional interface to use D3M primitives for your
own ML or AutoML programs. You can simply call `ml.d3m.fit`,
`ml.d3m.transform`, `ml.d3m.produce` and `ml.d3m.score` as shown below.
Functions to perform search and pipeline construction on the service
side will be added shortly. See the
[Pluros documentation](https://pluros.io/docs/pluros/topics/d3m) for more
information on these functions.

```python
import pluros as pl

model = pl.ml.d3m.fit("d3m.primitives.classification.svc.SKlearn", X, y)
y_pred = pl.ml.d3m.produce(model, X_test)
score = pl.ml.d3m.score("accuracy", y_test, y_pred).get()
```

In fact, if you are comfortable with the functional interface, you do not need
either the PlurosD3M package or the D3M package, which may simplify your
dependencies. (You will need the Pluros Python client however).


### The Proxy Primitive

PlurosD3M provides a generic proxy primitive on top of the simplified
functional interface. This proxy primitive behaves like a regular D3M TA1
primitive, except that its outputs are references to remote values held on the
Pluros service. To convert into an ordinary CallResult object, you must call
the `.get()` method.

```python
from plurosd3m.primitives import proxy_primitive

prim = proxy_primitive('d3m.primitives.classification.svc.SKlearn')
hp = prim.metadata.query()['primitive_code'] \
    ['class_type_arguments']['Hyperparams'].defaults()
prim_instance = prim(hyperparams=hp)
prim_instance.fit(X_train, y_train)
result = prim_instance.produce(X_test).get()
```

The `value` field of `proxy.CallResult` can be forwarded as inputs to another
`proxy_primitive` instance without fetching back the data, allowing you to
construct a pipeline without unnecessary data transfers.


### The older interface

The PlurosD3M package provides drop-in replacements for several D3M TA1
primitives. These primitives have the same primitive-ids as the original
primitives that they replace. Within the Pluros service, these primitives use
GPU implementations. While the primitives are generally semantically
equivalent, they are not likely to produce numerically identical results in any
given run. You can query the availability of a replacement primitive for any
D3M primitive using the following code:

```python
import d3m.index
import plurosd3m

prim = d3m.index.get_primitive('d3m.primitives.classification.svc.SKlearn')
hyperparams = prim.metadata.query()['primitive_code'] \
    ['class_type_arguments']['Hyperparams'].defaults()
opt_prim = plurosd3m.tools.get_optimized_primitive(prim, hyperparams)
```

In our experience, highly optimized GPU code often does not support as wide a
range of hyperparameters as CPU-based code. That is why you have to pass in a
set of hyperparameters to query whether a suitable replacement is available.

The replacement primitive can be used just like the original primitive. Note
that PlurosD3M replacement primitives currently fetch back results from the
service, so each application of a replacement primitive can have a fair bit of
data movement overhead. This will be rectified in a future version of this
package.

If you have constructed a D3M pipeline `ppln` or obtained one from a TA2
package, you can replace all replacable primitives in it with the following:

```python
import plurosd3m

plurosd3m.tools.optimize_pipeline(ppln, [hyperparams])
```

If you run into any problems using this pipeline, you can revert to the
original by calling `undo_optimize_pipeline`, which can be convenient in an
exception handler.

It is unclear if the older interface will be maintained if the Proxy Primitive
proves to be adequate.


Author
-------

PlurosD3M was developed by Vishal Vaidyanathan, Royal Caliber LLC.


Acknowledgements
----------------

The development of PlurosD3M was supported by DARPA SBIR award 140D6318C0093.
