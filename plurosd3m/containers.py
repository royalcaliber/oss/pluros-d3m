import io
import tarfile
import json

import pandas

import pluros as pl
import d3m.container.pandas

from .metadata import metadata_to_json, json_to_metadata


# Register API serialization and deserialization functions for D3M container
# types.

# For D3M DataFrames, we separately send up the DataFrame and its metadata and
# recombine it on the service side. This is because we don't load pickles
# within the service.
#
# This should be factored with the pluros.client implementation for pandas
# dataframes - TODO.
@pl.input.register(d3m.container.pandas.DataFrame)
def _(df):
    # Upload the pandas portion.
    tab = pl.input.dispatch(pandas.core.frame.DataFrame)(df)
    # Now upload the metadata
    mdjs = metadata_to_json(df.metadata)
    mddata = json.dumps(mdjs).encode('utf-8')
    md = pl.jsonData.fromData(data=pl.input(mddata), format='application/json')
    # Combine.
    return pl.ml.d3m.d3mDataFrame.fromTable(table=tab, metadata=md)


# Retrieving a D3M DataFrame requires a custom format because we can currently
# only get back a single data object from a .get() call. We get back a tarfile
# containing a parquet representation of the dataframe and JSON metadata as a
# second file.
@pl.deserialize('application/x.d3m_dataframe')
def _(data):
    tf = tarfile.open(fileobj=io.BytesIO(data), mode='r:gz')
    dfdata = tf.extractfile('df')
    df = pandas.read_parquet(dfdata)
    try:
        mdjs = json.load(tf.extractfile('md'))
        md = json_to_metadata(mdjs)
    except KeyError:
        md = None
    ret = d3m.container.pandas.DataFrame(df, generate_metadata=md is None)
    if md:
        ret.metadata = md
    return ret


# Support uploading loaded D3M Datasets. See options for uploading datasets
# without loading into memory.
@pl.input.register(d3m.container.dataset.Dataset)
def _(ds):
    # Upload the dictionary part. Note: this may exceed API policy limits if
    # the dictionary has too many elements, but typically datasets have a small
    # set of tables. If images etc. are uploaded as individual resources, this
    # could definitely be an issue.
    resources = pl.mapping.value(**ds)
    return pl.ml.d3m.dataset.fromResources(resources, ds.metadata)


# If we retrieve a Dataset from the service, we get back a tarfile with all the
# resources. We could automatically unpack and load it into memory, but that
# may or may not be what the typical user wants, especially with a larger
# dataset. So we do not define a deserialization function for Datasets.
