# Some supporting functions for de/serializing metadata that doesn't involve
# pickle since pickle is not allowed over the Pluros API.
#
# Not all aspects of metadata can survive a round-trip. In particular, we only
# support the types that are defined within the d3m core package. Any custom
# metadata types, hyperparams etc. that are defined in other D3M-related
# packages will not be correctly handled, as neither service nor client is
# expected to have matching installations. We do expect that the client is
# using a reasonably recent version of the d3m core package for this to work.


import enum
import logging
import inspect
import typing
import importlib
import json

import numpy

# We are using some private stuff from d3m for consistency.
from d3m.utils import _is_int, _is_float, is_instance
import d3m.metadata
from d3m.metadata.hyperparams import Hyperparams
from d3m.metadata.base import (
    ALL_ELEMENTS, NO_VALUE, ALL_ELEMENTS_TYPE, NO_VALUE_TYPE)

import pluros as pl


# VV - list of modules we are allowed to load during restoring stuff from json.
def _is_allowed_module(modname):
    return (modname.startswith('d3m.')
            or modname in {'builtins', 'typing', 'numpy', '_operator'})


# This function is intentionally written to mirror the original in d3m.utils.
def to_reversible_json_structure(obj):
    """ This is a modification of d3m.utils.to_reversible_json_structure that
    does not use pickle. Instead we restrict the number of things that can be
    restored to a small handful of core d3m types.
    """
    if isinstance(obj, (str, bool, type(None))):
        return obj

    obj_type = type(obj)

    if _is_int(obj_type):
        return int(obj)

    elif _is_float(obj_type):
        obj = float(obj)

        if not numpy.isfinite(obj):
            # Use a different encoding than pickle. This uses Python's str
            # function which means that any information embedded in a NaN will
            # be lost.
            return {
                'encoding': 'strflt',
                'value': str(obj)
            }

        else:
            return obj

    elif isinstance(obj, typing.Mapping):
        if 'encoding' in obj and 'value' in obj:
            return {
                'encoding': 'escape',
                'value': {k: to_reversible_json_structure(v)
                          for k, v in obj.items()}
            }
        else:
            return {k: to_reversible_json_structure(v) for k, v in obj.items()}

    elif obj_type in [tuple, list]:
        return [to_reversible_json_structure(v) for v in obj]

    # Handle enumerations without pickle. This only works for enumerations that
    # are within the d3m modules. Custom enumerations in other modules are not
    # supported because there is no expectation that those modules will be
    # present on both sides of the Pluros transaction.
    elif issubclass(obj_type, enum.Enum):
        modname = inspect.getmodule(obj_type).__name__
        if _is_allowed_module(modname):
            return {
                'encoding': 'enum',
                'module': modname,
                'type': obj_type.__qualname__,
                'value': obj.name
            }
        else:
            logging.warning(f'dropping enum {obj}')
            return None

    # Handle types. We only allow types that are defined within the d3m core
    # package along with a few from the standard library.
    elif inspect.isclass(obj):
        modname = inspect.getmodule(obj).__name__
        if _is_allowed_module(modname):
            return {
                'encoding': 'type',
                'module': modname,
                'value': obj.__qualname__
            }

        # For hyperparams, we save just the configuration. On round trip, the
        # type that is returned will be different, but with the same
        # configuration.
        elif issubclass(obj, Hyperparams):
            return {
                'encoding': 'hyperparams',
                'value': to_reversible_json_structure(obj.configuration)
            }

        else:
            # Typically types are used to constrain things. Rather than
            # returning None here and causing problems in validation, instead
            # we return object (or typing.Any?).
            #
            # Obviously this will not work if the type is being used to
            # instantiate something, but neither will None.
            logging.warning(f'dropping type {obj}')
            return {
                'encoding': 'type',
                'module': 'builtins',
                'value': 'object'
            }

    # Support things from typing that are not considered classes.
    #
    # This is probably a bug in python 3.6:
    # type(typing.Union[float]) returns type
    # type(typing.Union[float, str]) returns typing.Union
    # inspect.isclass(typing.Union[float]) returns True
    # inspect.isclass(typing.Union[float, str]) returns False
    elif inspect.getmodule(obj_type) is typing:
        name = obj_type.__qualname__
        # This is super hacky, but the typing module seems pretty hacky too...
        if name.startswith('_'):
            name = name[1:]
        if hasattr(obj, '__args__') and obj.__args__:
            value = [to_reversible_json_structure(v) for v in obj.__args__]
        else:
            value = None
        return {
            'encoding': 'typing',
            'type': name,
            'value': value
        }

    # Hyperparams
    elif issubclass(obj_type, d3m.metadata.hyperparams.Hyperparameter):
        modname = inspect.getmodule(obj).__name__
        if not _is_allowed_module(modname):
            logging.warning(f'dropping hyperparameter {obj_type}')
            return None

        # We save all the instance variables in a dictionary and directly
        # restore it later. This only works if slots are not used for
        # hyperparams, which is the case presently.
        return {
            'encoding': 'hyperparam',
            'module': modname,
            'type': obj_type.__qualname__,
            'value': to_reversible_json_structure(obj.__dict__)
        }

    # Some special cases needed for metadata
    elif obj_type in [ALL_ELEMENTS_TYPE, NO_VALUE_TYPE]:
        return {
            'encoding': 'special',
            'value': obj_type.__qualname__
        }

    # Some free functions and operators.
    elif callable(obj):
        modname = inspect.getmodule(obj).__name__
        if _is_allowed_module(modname):
            return {
                'encoding': 'callable',
                'module': modname,
                'value': obj.__name__
            }
        else:
            logging.warn(f'dropping callable {obj}')
            return None

    # Some numpy arrays used in hyperparams. For now we just convert to a list
    # and back. But we could also use numpy.save/load if needed (without pickle
    # on the load of course).
    elif isinstance(obj, numpy.ndarray):
        # Only 1d supported right now.
        if obj.ndim != 1:
            logging.warn(f'dropping numpy array of dimension {obj.ndim}')
            return None
        return {
            'encoding': 'numpy',
            'value': to_reversible_json_structure(list(obj))
        }

    else:
        logging.warning(f'dropping object of type {obj_type}')
        return None


# Load a module from an allowed list and return the loaded module object.
def _load_module_safe(mod):
    if not _is_allowed_module(mod):
        raise ValueError(
            f'module {mod} is not permitted to be loaded via json')
    return importlib.import_module(mod)


def from_reversible_json_structure(obj):
    if isinstance(obj, (str, int, float, bool, type(None))):
        return obj

    elif isinstance(obj, typing.Mapping):
        if 'encoding' in obj and 'value' in obj:
            encoding = obj['encoding']
            if encoding == 'pickle':
                logging.warning('ignoring pickle in json structure')
                return None
            elif encoding == 'strflt':
                return float(obj['value'])
            elif encoding == 'enum':
                mod_obj = _load_module_safe(obj['module'])
                enum_type = mod_obj.__dict__[obj['type']]
                return enum_type[obj['value']]
            elif encoding == 'type':
                # special case for types that are not ordinarily declared
                if obj['value'] == 'NoneType':
                    return type(None)
                mod_obj = _load_module_safe(obj['module'])
                return mod_obj.__dict__[obj['value']]
            elif encoding == 'typing':
                value = obj['value']
                if value is not None:
                    args = tuple(from_reversible_json_structure(v)
                                 for v in obj['value'])
                    return typing.__dict__[obj['type']][args]
                else:
                    return typing.__dict__[obj['type']]
            elif encoding == 'hyperparams':
                return Hyperparams.define(
                    from_reversible_json_structure(obj['value']))
            elif encoding == 'hyperparam':
                mod_obj = _load_module_safe(obj['module'])
                T = mod_obj.__dict__[obj['type']]
                hp = T.__new__(T)
                hp.__dict__.update(
                    from_reversible_json_structure(obj['value']))
                return hp
#            elif encoding == 'tss':
#                return _hp_from_tss(
#                    from_reversible_json_structure(obj['value']))
            elif encoding == 'escape':
                return from_reversible_json_structure(obj['value'])
            elif encoding == 'special':
                if obj['value'] == 'ALL_ELEMENTS_TYPE':
                    return ALL_ELEMENTS
                elif obj['value'] == 'NO_VALUE_TYPE':
                    return NO_VALUE
            elif encoding == 'callable':
                mod_obj = _load_module_safe(obj['module'])
                return mod_obj.__dict__[obj['value']]
            elif encoding == 'numpy':
                return numpy.array(
                    from_reversible_json_structure(obj['value']))
            else:
                raise ValueError(f"Unsupported encoding '{encoding}'.")
        else:
            return {k: from_reversible_json_structure(v)
                    for k, v in obj.items()}

    # We do not use "is_sequence" because we do not want to convert all
    # sequences, because it can be loosing important information.
    #
    # This needs to return a tuple rather than a list because there is
    # something wrong with the typing system, at least in 3.6: [100] is not
    # considered an instance of typing.Sequence, but (100,) is.
    elif type(obj) in [tuple, list]:
        return tuple(from_reversible_json_structure(v) for v in obj)

    else:
        raise TypeError(f"Unsupported type '{type(obj)}'")


def metadata_to_json(md):
    """ Converts D3M metadata object into a JSON form that can be reversed into
    a roughly equivalent metadata object. Unlike the stock d3m functions, this
    does not rely on pickling.
    """

    # We use a quick hack: we use the to_internal_json function to create a
    # JSON with pickle nodes in it. Then we replace them with something more
    # palatable to us.

    # Record the most derived Metadata type from the standard types so we can
    # restore accordingly.
    mod = d3m.metadata.base
    if isinstance(md, mod.PrimitiveMetadata):
        type = mod.PrimitiveMetadata
    elif isinstance(md, mod.DataMetadata):
        type = mod.DataMetadata
    elif isinstance(md, mod.Metadata):
        return mod.Metadata
    else:
        raise TypeError(f'expected a Metadata or derived instance')

    return to_reversible_json_structure({
        'type': type,
        'value': d3m.metadata.base.Metadata.to_internal_simple_structure(md)
    })


def json_to_metadata(js):
    """ Inverts the json_to_metadata function to recover as close an
    approximation of the original metadata object that was serialized as
    possible.
    """
    mod = d3m.metadata.base
    obj = from_reversible_json_structure(js)
    # As a security precaution, we ensure that the type is one of the types we
    # could have serialized.
    T = obj['type']
    if T is mod.PrimitiveMetadata:
        md = T()
        for entry in obj['value']:
            md = md.update(entry['metadata'])
        return md
    elif T in [mod.Metadata, mod.DataMetadata]:
        return T.from_internal_simple_structure(obj['value'])
    else:
        raise TypeError(f'unexpected type {T}')


# Register an input function for Metadata:
@pl.input.register(d3m.metadata.base.Metadata)
def _(md):
    mdjs = metadata_to_json(md)
    mddata = json.dumps(mdjs).encode('utf-8')
    js = pl.jsonData.fromData(data=pl.input(mddata))
    return pl.ml.d3m.metadata.fromJSON(js)


# Deserialize to DataMetadata. Data comes back as json.
@pl.deserialize('application/x.d3m_metadata')
def _(data):
    return json_to_metadata(json.loads(data.decode('utf-8')))
