# JHU graph embedding primitives.
#
# To avoid software complexity, these primitives represent an "inlining" of all
# the relevant code from graspy and jhu-primitives. Code snippets from the
# original are provided here under the terms of the Apache License v2.0.

from typing import Optional, Any, Dict, List
import math
import numpy as np

from scipy.stats import rankdata
import networkx as nx

from d3m.metadata import params, hyperparams
from d3m.metadata.base import (
    PrimitiveMetadata,
    PrimitiveAlgorithmType,
    PrimitiveFamily,
    PrimitiveInstallationType)
from d3m.container import DataFrame
from d3m.exceptions import PrimitiveNotFittedError
from d3m.primitive_interfaces.unsupervised_learning \
    import UnsupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import DockerContainer
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult
import d3m.container as container

import pluros as pl
import pluros.numpy_compat as pnp


# This function is taken verbatim from the JHU original
# graspy.embed.svd._compute_likelihood.
def _compute_likelihood(arr):
    """
    Computes the log likelihoods based on normal distribution given
    a 1d-array of sorted values. If the input has no variance,
    the likelihood will be nan.
    """
    n_elements = len(arr)
    likelihoods = np.zeros(n_elements)

    for idx in range(1, n_elements + 1):
        # split into two samples
        s1 = arr[:idx]
        s2 = arr[idx:]

        # deal with when input only has 2 elements
        if (s1.size == 1) & (s2.size == 1):
            likelihoods[idx - 1] = -np.inf
            continue

        # compute means
        mu1 = np.mean(s1)
        if s2.size != 0:
            mu2 = np.mean(s2)
        else:
            # Prevent numpy warning for taking mean of empty array
            mu2 = -np.inf

        # compute pooled variance
        variance = ((np.sum((s1 - mu1) ** 2) + np.sum((s2 - mu2) ** 2))) / (
            n_elements - 1 - (idx < n_elements)
        )
        std = np.sqrt(variance)

        # compute log likelihoods
        likelihoods[idx - 1] = np.sum(norm.logpdf(s1, loc=mu1, scale=std)) + np.sum(
            norm.logpdf(s2, loc=mu2, scale=std)
        )

    return likelihoods


# From graspy.utils.utils.
def is_unweighted(X):
    return ((X == 0) | (X == 1)).all()


# From graspy.utils.utils, modified to handle sparse.
def is_symmetric(X):
    return np.all(X == X.T)


# From graspy.utils.utils, modified to work with scipy sparse.
def is_loopless(X):
    return not np.any(X.diagonal() != 0)


# This is a modification of the original from graspy.utils.ptr to handle sparse
# matrices. The graph should be a scipy sparse matrix.
def pass_to_ranks(graph, method="simple-nonzero"):
    r"""
    Rescales edge weights of an adjacency matrix based on their relative rank in
    the graph.

    Parameters
    ----------
    graph: Adjacency matrix

    method: {'simple-nonzero' (default), 'simple-all', 'zero-boost'} string, optional

        - 'simple-nonzero'
            assigns ranks to all non-zero edges, settling ties using
            the average. Ranks are then scaled by
            :math:`\frac{rank(\text{non-zero edges})}{\text{total non-zero edges} + 1}`
        - 'simple-all'
            assigns ranks to all non-zero edges, settling ties using
            the average. Ranks are then scaled by
            :math:`\frac{rank(\text{non-zero edges})}{n^2 + 1}`
            where n is the number of nodes
        - 'zero-boost'
            preserves the edge weight for all 0s, but ranks the other
            edges as if the ranks of all 0 edges has been assigned. If there are
            10 0-valued edges, the lowest non-zero edge gets weight 11 / (number
            of possible edges). Ties settled by the average of the weight that those
            edges would have received. Number of possible edges is determined
            by the type of graph (loopless or looped, directed or undirected).



    See also
    --------
    scipy.stats.rankdata

    Returns
    -------
    graph: numpy.ndarray, shape(n_vertices, n_vertices)
        Adjacency matrix of graph after being passed to ranks
    """

    # Check if the graph is unweighted
    if is_unweighted(graph.data):
        return graph

    if graph.data.min() < 0:
        raise UserWarning(
            "Current pass-to-ranks on graphs with negative"
            + " weights will yield nonsensical results, especially for zero-boost"
        )

    n = graph.shape[0]
    non_zeros = graph.data[graph.data != 0]
    # Even in sparse representation, some edges may have an explicit zero.
    nnz = len(non_zeros)
    nz = n * n - nnz
    rank = rankdata(non_zeros)
    if method == "zero-boost":
        sym = is_symmetric(graph)
        if sym:
            if is_loopless(graph):
                num_zeros = (nz - n) / 2
                possible_edges = n * (n - 1) / 2
            else:
                # VV: this seems wrong?
                num_zeros = (
                    nz / 2 - n * (n - 1) / 2
                )
                possible_edges = n * (n + 1) / 2
        else:
            if is_loopless(graph):
                # n^2 - num_nonzero - num_diagonal
                num_zeros = n * n - nnz - graph.shape[0]
                # n^2 - num_diagonal
                possible_edges = n * n - n
            else:
                num_zeros = nz
                possible_edges = graph.size

        # shift up by the number of zeros
        rank = rank + num_zeros
        # normalize by the number of possible edges for this kind of graph
        rank = rank / possible_edges
        # put back into matrix form (and reflect over the diagonal if necessary)
        graph.data[graph.data != 0] = rank
        if sym:
            # Symmetrize
            graph = (graph + graph.T) / 2
        return graph
    elif method in ["simple-all", "simple-nonzero"]:
        if method == "simple-all":
            normalizer = n * n
        elif method == "simple-nonzero":
            normalizer = n
        rank = rank / (normalizer + 1)
        graph.data[graph.data != 0] = rank
        return graph
    else:
        raise ValueError("Unsuported pass-to-ranks method")


def _do_svd(X, k, omni):
    if omni:
        D, u, v = pnp.asarrays(pl.ml.d3m.jhuOmniSVD(X, k, _nOut=3))
    else:
        D, u, v = pnp.asarrays(pl.array.linalg.tsvd(X, k, _nOut=3))

    # Fetch back everything because most numpy operations are not available in
    # Pluros Lite yet.
    D, u, v = pl.get(D, u, v)
    # D will already be sorted from pluros SVD.
    return D, u, v


# See graspy.embed.svd.select_dimension.
#
# This differs in that we return the decomposition rather than the elbow.
#
# If omni is True, X should be a list of graphs to be embedded.
def svd(X, k=None, n_elbows=None, omni=False):
    # Compute k0 which is the number of svs we find. If k is given, then k0 is
    # k. Otherwise k is something larger from which we find the correct number
    # of elbows.
    if k is None:
        assert n_elbows is not None
        if omni:
            k0 = math.ceil(math.log2(len(X) * min(X[0].shape)))
        else:
            k0 = math.ceil(math.log2(min(X.shape)))
    else:
        k0 = k

    D, u, v = _do_svd()(X, k0, omni=omni)

    # Adapted from the original: discarding all the stuff that we don't
    # actually use afterwards.
    elbow = 0
    for _ in range(n_elbows):
        arr = D[elbow:]
        if arr.size <= 1:  # Cant compute likelihoods with 1 numbers
            break
        lq = _compute_likelihood(arr)
        idx += np.argmax(lq) + 1
        elbow = idx

    # Return only the required components.
    return D[:elbow], u[:, :elbow], v[:, :elbox]


# See graspy.embed.svd.selectSVD
#
# This function differs from the original in that it never performs a full SVD,
# which would be too expensive for larger graphs. If `n_components` is not
# given, we repeatedly try to guess the number of components until we find the
# designated number of elbows.
def selectSVD(X, n_components=None, n_elbows=2, algorithm='randomized',
              n_iter=5, omni=False):
    if algorithm not in ['randomized', 'irlb']:
        msg = 'algorithm must be one of {randomized, irlb}'

    if n_components is None:
        s, u, v = auto_svd(X, n_elbows, omni=omni)
    else:
        s, u, v = _do_svd(X, k=n_components, omni=omni)

    # In the order that graspy returns it.
    return u, s, v


# Performs the spectral embedding of the adjacency matrix x.
def _ase(x, n_components, n_elbows):
    u, s, vT = selectSVD(x, n_components, n_elbows)
    # Assume the graph is always directed. In future we will have efficient
    # Pluros functions to test for symmetry.
    return np.concatenate((u @ np.diag(np.sqrt(s)), vT @ np.diag(np.sqrt(s))))


def _omni(x, n_components, n_elbows, n_graphs, n_nodes):
    u, s, vT = selectSVD(x, n_components, n_elbows, omni=True)
    left = (u @ np.diag(np.sqrt(s))).reshape(n_graphs, n_nodes, -1)
    right = (vT @ np.diag(np.sqrt(s))).reshape(n_graphs, n_nodes, -1)
    return np.concatenate(left, right)


# Shorthand.
_stypes = ['https://metadata.datadrivendiscovery.org/types/TuningParameter']


class ASEHyperparams(hyperparams.Hyperparams):
    max_dimension = hyperparams.Bounded[int](
        default=2,
        lower=1,
        upper=None,
        semantic_types=_stypes
    )

    which_elbow = hyperparams.Bounded[int](
        default=1,
        lower=1,
        upper=2,
        semantic_types=_stypes
    )

    use_attributes = hyperparams.Hyperparameter[bool](
        default=False,
        semantic_types=_stypes
    )


# This is adapted from the JHU ASE primitive, with some differences noted
# below.
#
# Note: attributes are taken from the node table (first entry in the inputs
# list), not from the networkx graph attributes.
#
# Note: The kernel matrix is maintained sparse by only computing the rbf_kernel
# for edges that exist in the graph, not for all node pairs.
#
# Note: this primitive does not support Pluros graphs as inputs and produces
# local arrays as outputs (not Pluros values). This is due to some temporary
# limitations on the Pluros side (some unimplemnented numpy functions).
class PlurosASE(TransformerPrimitiveBase[container.List,
                                         container.List,
                                         ASEHyperparams]):
    """Adjacency spectral embedding, modeled after JHU ASE primitive.
    """
    __author__ = 'Royal Caliber'
    metadata = PrimitiveMetadata({
        "algorithm_types":
            [PrimitiveAlgorithmType.SINGULAR_VALUE_DECOMPOSITION],
        "name": "PlurosASE",
        "primitive_family": PrimitiveFamily.DATA_TRANSFORMATION,
        "python_path": ("d3m.primitives.data_transformation."
                        "adjacency_spectral_embedding.Pluros"),
        "source": {
            'name': 'Royal Caliber',
            'contact': 'mailto:vishal@royal-caliber.com',
            'uris': ['https://gitlab.com/royalcaliber/oss/pluros-d3m']
        },
        "version": "2022.01.24",
        "id": "122d1099-0cf5-435f-b1cc-a7c68982188b",
        "hyperparams_to_tune": ['which_elbow'],
        'installation': [{
            'type': PrimitiveInstallationType.PIP,
            'package': 'pluros-d3m',
            'version': '>=0.0.0',
        }]
    })

    def produce(self, *, inputs: container.List,
                timeout: float = None,
                iterations: int = None) -> CallResult[container.List]:
        # unpacks necessary input arguments
        # note that other inputs are just passed through !
        learning_data = inputs[0]
        graphs_all = inputs[1]
        task = inputs[3]

        # ase only works for one graph (but we can change that)
        # VV: No unnecessary copy here.
        graph = graphs_all[0]

        n_elbows = self.hyperparams['which_elbow']
        max_dimension = self.hyperparams['max_dimension']
        use_attributes = self.hyperparams['use_attributes']

        # catches link-prediction problem type
        # if it is not such - applies pass to ranks, which is a method to
        # rescale edge weights based on their relative ranks
        if task == 'linkPrediction':
            graph_adjacency = nx.adjacency_matrix(graph)
            use_attributes = False
        else:
            graph_adjacency = pass_to_ranks(nx.adjacency_matrix(graph))

        n = graph_adjacency.shape[0]

        # it is counter-intuitive to embed into more dimensions than the
        # original
        if max_dimension > n:
            max_dimension = n

        if use_attributes:
            # Get attribute names from the node table.
            attributes_names = set(learning_data.columns)
            # check if there are any attributes, other than nodeIDs
            # attributes_names = set([k for n in graph.nodes for k in graph.nodes[n].keys()])
            attributes_names.discard('nodeID')

        if use_attributes and len(attributes_names):
            # Compute sparse rbf_kernel only for edges.
            coo = graph_adjacency.tocoo()
            src_attrs = learning_data.iloc[coo[:, 0]][attribute_names]
            dst_attrs = learning_data.iloc[coo[:, 1]][attribute_names]
            gamma = 1.0 / len(attribute_names) / 2
            coo.data = np.exp(-gamma * np.square(src_attrs - dst_attrs))
            # We don't know if order is preserved in going from csr to coo,
            # otherwise we can avoid this.
            kernel_matrix = coo.tocsr()

            adjacencies = [graph_adjacency, kernel_matrix]
            embedding = _omni(
                adjacencies,
                n_components=max_dimension,
                n_elbows=n_elbows)

            #omni = graspyOMNI(n_components = max_dimension, n_elbows = n_elbows)
            #embedding = np.array(omni.fit_transform(adjacencies))
            if embedding.ndim == 4: # directed inputs
                embedding = np.mean(embedding, axis=1)
                embedding = np.concatenate(embedding, axis=1)
            else: # undirected inputs
                embedding = np.mean(embedding, axis=0)

            inputs[1][0] = container.ndarray(embedding)
            return CallResult(inputs)

        else:
            embedding = _ase(
                graph_adjacency, n_components=max_dimension,
                n_elbows = n_elbows)

            inputs[1][0] = container.ndarray(embedding)
            return CallResult(inputs)
