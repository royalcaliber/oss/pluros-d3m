from typing import Optional, Any, Dict, List

from d3m.metadata import params, hyperparams
from d3m.metadata.base import (
    PrimitiveMetadata,
    PrimitiveAlgorithmType,
    PrimitiveFamily,
    PrimitiveInstallationType)
from d3m.container import DataFrame
from d3m.exceptions import PrimitiveNotFittedError
from d3m.primitive_interfaces.unsupervised_learning \
    import UnsupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer

import pluros as pl
from plurosd3m.primitives.proxy import CallResult
from plurosd3m.primitives.plprim.util import (
    d3m_df_to_array, d3m_df_get_col_names)


# Shorthand.
_stypes = ['https://metadata.datadrivendiscovery.org/types/TuningParameter']


# Params support to be added later if needed. Right now we just hold on to the
# model itself.
class Params(params.Params):
    centers: Optional[Any]

    
class Hyperparams(hyperparams.Hyperparams):
    n_clusters = hyperparams.Bounded[int](
        default=10,
        lower=1,
        upper=None,
        description='Number of centroids',
        semantic_types=_stypes
    )
    
    n_trials = hyperparams.Bounded[int](
        default=10,
        lower=1,
        upper=None,
        description='Number of independent trials',
        semantic_types=_stypes
    )

    max_iter = hyperparams.Bounded[int](
        default=300,
        lower=1,
        upper=None,
        description='Maximum number of iterations',
        semantic_types=_stypes
    )

    tol = hyperparams.Bounded[float](
        default=1e-4,
        lower=0,
        upper=1,
        description='Relative convergence threshold',
        semantic_types=_stypes
    )


class PlurosKMeans(UnsupervisedLearnerPrimitiveBase[DataFrame,
                                                    DataFrame,
                                                    Params,
                                                    Hyperparams]):
    """K-Means Clustering.
    """
    __author__ = 'Royal Caliber'
    metadata = PrimitiveMetadata({
        "algorithm_types": [PrimitiveAlgorithmType.K_MEANS_CLUSTERING],
        "name": "PlurosKMeans",
        "primitive_family": PrimitiveFamily.CLUSTERING,
        "python_path": "d3m.primitives.clustering.k_means.Pluros",
        "source": {
            'name': 'Royal Caliber',
            'contact': 'mailto:vishal@royal-caliber.com',
            'uris': ['https://gitlab.com/royalcaliber/oss/pluros-d3m']
        },
        "version": "2022.01.18",
        "id": "4eea0b6e-7871-4fac-b248-b629de0cc476",
        "hyperparams_to_tune": ['n_trials', 'max_iter', 'tol'],
        'installation': [{
            'type': PrimitiveInstallationType.PIP,
            'package': 'pluros-d3m',
            'version': '>=0.0.0',
        }]
    })


    def __init__(self,
                 *,
                 hyperparams: Hyperparams,
                 random_seed: int=0,
                 docker_containers: Dict[str, DockerContainer]=None):
        super().__init__(
            hyperparams=hyperparams,
            random_seed=random_seed,
            docker_containers=docker_containers)
        self._inputs = None
        self._fitted = False
        self._centers = None

    def set_training_data(self, *, inputs: DataFrame):
        self._inputs = inputs
        self._fitted = False

    def fit(self,
            *,
            timeout: float=None,
            iterations: int=None) -> CallResult:
        if self._inputs is None:
            raise ValueError('Missing training data')
        # Convert to array    
        x = d3m_df_to_array(self._inputs, dtype='f')
        # Call Pluros function to cluster
        hp = self.hyperparams
        # Only centers are retained. For fit-produce, labels should be returned.
        # TODO.
        labels, self._centers, var, conv = pl.ml.clustering.kmeans.cluster(
            x,
            k=hp['n_clusters'],
            nTrials=hp['n_trials'],
            maxIter=hp['max_iter'],
            tol=hp['tol'],
            _nOut=4)
        self._fitted = True
        return CallResult(None)

    def produce(self,
                *,
                inputs: DataFrame,
                timeout: float=None,
                iterations: int=None) -> CallResult:
        # Convert to array            
        x = d3m_df_to_array(inputs, dtype='f')
        # Transform
        out = pl.ml.clustering.kmeans.transform(self._centers, x)
        # Convert back to a table.
        out = pl.ml.d3m.d3mDataFrame.fromArray(out)
        return CallResult(out)

    def get_params(self) -> Params:
        return Params(centers=self._centers)

    def set_params(self, *, params: Params) -> None:
        self._centers = params['centers']
        self._fitted = self._centers is not None
