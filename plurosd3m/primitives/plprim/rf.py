from typing import Optional, Any, Dict, List

from d3m.metadata import params, hyperparams
from d3m.metadata.base import (
    PrimitiveMetadata,
    PrimitiveAlgorithmType,
    PrimitiveFamily,
    PrimitiveInstallationType)
from d3m.container import DataFrame
from d3m.exceptions import PrimitiveNotFittedError
from d3m.primitive_interfaces.supervised_learning \
    import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer

import pluros as pl
from plurosd3m.primitives.proxy import CallResult
from plurosd3m.primitives.plprim.util import (
    d3m_df_to_array, d3m_df_get_col_names, d3m_df_get_dtypes)


# Shorthand.
_stypes = ['https://metadata.datadrivendiscovery.org/types/TuningParameter']


# Params support to be added later if needed. Right now we just hold on to the
# model itself.
class Params(params.Params):
    model: Optional[Any]
    col_names: Optional[Any]
    col_types: Optional[Any]
    classes: Optional[Any]


# Tunable hyperparameters common to both classification and regression.
class _Hyperparams(hyperparams.Hyperparams):
    n_estimators = hyperparams.Bounded[int](
        default=10,
        lower=1,
        upper=None,
        description='Number of trees in the forest.',
        semantic_types=_stypes,
    )

    max_depth = hyperparams.Bounded[int](
        default=-1,
        lower=-1,
        upper=None,
        description='The maximum depth of the tree.',
        semantic_types=_stypes,
    )

    sample_frac = hyperparams.Bounded[float](
        default=1.0,
        lower=0.0,
        upper=1.0,
        description='Fraction of samples to be used for each tree',
        semantic_types=_stypes
    )

    feature_frac = hyperparams.Bounded[float](
        default=1.0,
        lower=0.0,
        upper=1.0,
        description='Fraction of features to be used for each split',
        semantic_types=_stypes
    )

    bootstrap = hyperparams.UniformBool(
        default=True,
        description="Use bootstrapping for each tree",
        semantic_types=_stypes
    )

    max_leaves = hyperparams.Bounded[int](
        default=-1,
        lower=-1,
        upper=None,
        description="Maximum number of leaves in a tree, -1 means no limit.",
        semantic_types=_stypes
    )

    min_split_samples = hyperparams.Bounded[int](
        default=2,
        lower=2,
        upper=None,
        description=(
            "Minimum number of samples for a node to considered"
            " for splitting"),
        semantic_types=_stypes
    )

    min_impurity_decrease = hyperparams.Bounded[float](
        default=0,
        lower=0,
        upper=None,
        description=(
            "Minimum decrease in split criterion for a split to be done."),
        semantic_types=_stypes
    )


# Hyperparams for classification only.
class CHyperparams(_Hyperparams):
    split_criterion = hyperparams.Enumeration[str](
        values=['gini', 'entropy'],
        default='gini',
        description=('Criterion used for splitting a tree node,'
                     ' should be one of "gini", "entropy"'),
        semantic_types=_stypes
    )


# Hyperparams for regression only.
class RHyperparams(_Hyperparams):
    split_criterion = hyperparams.Enumeration[str](
        values=['mse', 'mae'],
        default='mse',
        description=('Criterion used for splitting a tree node,'
                     ' should be one of "mse" or "mae"'),
        semantic_types=_stypes
    )


class _RFCommon:
        def __init__(self, type):
            self._type = type
            self._inputs = None
            self._outputs = None
            self._fitted = False
            self._model = None
            # Remember column names and types for fitted models. For
            # classification, inputs are not always ordinal encoded, so
            # remember the classes.
            self._col_names = None
            self._col_types = None
            self._classes = None

        def _set_training_data(self, *, inputs: DataFrame, outputs: DataFrame):
                self._inputs = inputs
                self._outputs = outputs
                self._fitted = False

        def _fit(self, *, timeout: float = None, iterations: int = None):
            if self._inputs is None or self._outputs is None:
                raise ValueError('Missing training data')
            hp = self.hyperparams
            if self._type == 'c':
                f = pl.ml.tree.rfc.train
            else:
                f = pl.ml.tree.rfr.train

            # For compatibility with Sklearn, which handles non-encoded
            # categorical targets.
            if self._type == 'c':
                y, self._classes = pl.ml.d3m.d3mDataFrame.catEncode(
                    self._outputs, _nOut=2)
                y = d3m_df_to_array(y, 'i', squeeze=True)
            else:
                y = d3m_df_to_array(self._outputs, 'i', squeeze=True)

            self._model = f(
                x=d3m_df_to_array(self._inputs, 'f'),
                y=y,
                nTrees=hp['n_estimators'],
                maxDepth=hp['max_depth'],
                splitCriterion=hp['split_criterion'],
                sampleFrac=hp['sample_frac'],
                bootstrap=hp['bootstrap'],
                maxLeaves=hp['max_leaves'],
                minSplitSamples=hp['min_split_samples'],
                minImpurityDecrease=hp['min_impurity_decrease'],
            )
            # Grab the column name so we can produce outputs with the same
            # name.
            self._col_names = d3m_df_get_col_names(self._outputs)
            self._fitted = True
            return CallResult(None)

        def _produce(self,
                     *,
                     inputs: DataFrame,
                     timeout: float=None,
                     iterations: int=None) -> CallResult:
            if not self._fitted:
                raise PrimitiveNotFittedError('primitive is not fitted')
            # The SVM function only takes an Array, so we have to project down
            # to a Real Array and then convert back into a DataFrame.
            x = d3m_df_to_array(inputs, dtype='f')
            if self._type == 'c':
                f = pl.ml.tree.rfc.predict
            else:
                f = pl.ml.tree.rfr.predict
            pred = f(self._model, x)
            # Turn back into a DataFrame with column names and same dtype as
            # before. Any categorical columns we encoded are mapped back to
            # labels. We should also add back any metadata that is expected -
            # TODO.
            pred = pl.ml.d3m.d3mDataFrame.fromArray(
                pred, columnNames=self._col_names, classes=self._classes)
            return CallResult(pred)

        def _get_params(self) -> Params:
            if self._fitted:
                return Params(model=self._model, col_names=self._col_names,
                              col_types=self._col_types, classes=self._classes)
            else:
                return Params(model=None, col_names=None, col_types=None,
                              classes=self._classes)

        def _set_params(self, *, params: Params) -> None:
            self._model = params['model']
            self._col_names = params['col_names']
            self._col_types = params['col_types']
            self._classes = params['classes']
            self._fitted = self._model is not None


class PlurosRFC(SupervisedLearnerPrimitiveBase[DataFrame,
                                               DataFrame,
                                               Params,
                                               CHyperparams], _RFCommon):
        """ Primitive wrapping for Pluros functions
        [`ml.tree.rfc.*`](https://pluros.io/doc/pluros/functions/ml.html#ml.tree.rfc).
        """

        __author__ = 'Royal Caliber'

        metadata = PrimitiveMetadata({
            "algorithm_types": [PrimitiveAlgorithmType.RANDOM_FOREST],
            "name": "PlurosRFC",
            "primitive_family": PrimitiveFamily.CLASSIFICATION,
            "python_path":
                    "d3m.primitives.classification.random_forest.Pluros",
            "source": {
                'name': 'Royal Caliber',
                'contact': 'mailto:vishal@royal-caliber.com',
                'uris': ['https://gitlab.com/royalcaliber/oss/pluros-d3m']
            },
            "version": "2022.01.07",
            "id": "dcd30d35-2230-4fe2-a37e-5f16f24fcf0c",
            "hyperparams_to_tune": ['n_estimators', 'max_depth'],
            'installation': [{
                'type': PrimitiveInstallationType.PIP,
                'package': 'pluros-d3m',
                'version': '>=0.0.0',
            }]
        })

        def __init__(self,
                     *,
                     hyperparams: CHyperparams,
                     random_seed: int=0,
                     docker_containers: Dict[str, DockerContainer]=None):
            super().__init__(
                hyperparams=hyperparams,
                random_seed=random_seed,
                docker_containers=docker_containers)
            _RFCommon.__init__(self, type='c')

        def set_training_data(self, *, inputs: DataFrame, outputs: DataFrame):
            return self._set_training_data(inputs=inputs, outputs=outputs)

        def fit(self, *, timeout: float = None, iterations: int = None):
            return self._fit(timeout=timeout, iterations=iterations)

        def get_params(self) -> Params:
            return self._get_params()

        def produce(self,
                    *,
                    inputs: DataFrame,
                    timeout: float=None,
                    iterations: int=None) -> CallResult:
            return self._produce(inputs=inputs, timeout=timeout,
                                 iterations=iterations)

        def set_params(self, *, params: Params) -> None:
            return self._set_params(params=params)


class PlurosRFR(SupervisedLearnerPrimitiveBase[DataFrame,
                                               DataFrame,
                                               Params,
                                               RHyperparams], _RFCommon):
        """ Primitive wrapping for Pluros functions
        [`ml.tree.rfr.*`](https://pluros.io/doc/pluros/functions/ml.html#ml.tree.rfr).
        """

        __author__ = 'Royal Caliber'

        metadata = PrimitiveMetadata({
            "algorithm_types": [PrimitiveAlgorithmType.RANDOM_FOREST],
            "name": "PlurosRFR",
            "primitive_family": PrimitiveFamily.REGRESSION,
            "python_path": "d3m.primitives.regression.random_forest.Pluros",
            "source": {
                'name': 'Royal Caliber',
                'contact': 'mailto:vishal@royal-caliber.com',
                'uris': ['https://gitlab.com/royalcaliber/oss/pluros-d3m']
            },
            "version": "2022.01.07",
            "id": "6b3e0113-5439-4efe-aa16-84bcaa75bd66",
            "hyperparams_to_tune": ['n_estimators', 'max_depth'],
            'installation': [{
                'type': PrimitiveInstallationType.PIP,
                'package': 'pluros-d3m',
                'version': '>=0.0.0',
            }]
        })

        def __init__(self,
                     *,
                     hyperparams: RHyperparams,
                     random_seed: int=0,
                     docker_containers: Dict[str, DockerContainer]=None):
            super().__init__(
                hyperparams=hyperparams,
                random_seed=random_seed,
                docker_containers=docker_containers)
            _RFCommon.__init__(self, type='r')

        def set_training_data(self, *, inputs: DataFrame, outputs: DataFrame):
            return self._set_training_data(inputs=inputs, outputs=outputs)

        def fit(self, *, timeout: float = None, iterations: int = None):
            return self._fit(timeout=timeout, iterations=iterations)

        def get_params(self) -> Params:
            return self._get_params()

        def produce(self,
                    *,
                    inputs: DataFrame,
                    timeout: float=None,
                    iterations: int=None) -> CallResult:
            return self._produce(inputs=inputs, timeout=timeout,
                                 iterations=iterations)

        def set_params(self, *, params: Params) -> None:
            return self._set_params(params=params)
