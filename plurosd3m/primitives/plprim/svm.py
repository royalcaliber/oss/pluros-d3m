from typing import Optional, Any, Dict

from d3m.metadata import params, hyperparams
from d3m.metadata.base import (
    PrimitiveMetadata,
    PrimitiveAlgorithmType,
    PrimitiveFamily,
    PrimitiveInstallationType)
from d3m.container import DataFrame
from d3m.exceptions import PrimitiveNotFittedError
from d3m.primitive_interfaces.supervised_learning \
    import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer

import pluros as pl
from plurosd3m.primitives.proxy import CallResult
from plurosd3m.primitives.plprim.util import (
    d3m_df_to_array, d3m_df_get_col_names, d3m_df_get_dtypes)


# Shorthand.
_stypes = ['https://metadata.datadrivendiscovery.org/types/TuningParameter']


# Params support to be added later if needed. Right now we just hold on to the
# model itself.
class Params(params.Params):
    model: Optional[Any]
    col_names: Optional[Any]
    col_types: Optional[Any]
    classes: Optional[Any]


# Tunable hyperparameters
class Hyperparams(hyperparams.Hyperparams):
    C = hyperparams.Bounded[float](
        default=1,
        lower=0,
        upper=None,
        semantic_types=_stypes,
        description='Penalty parameter C of the error term.'
    )

    kernel = hyperparams.Enumeration[str](
        values=['linear', 'poly', 'rbf', 'sigmoid'],
        default='rbf',
        description=(
            'Specifies the kernel type to be used in the algorithm. It must'
            ' be one of \'linear\', \'poly\', \'rbf\', \'sigmoid\'. The'
            ' default kernel is \'rbf\'.'),
        semantic_types=_stypes
    )

    degree = hyperparams.Bounded[int](
        default=3,
        lower=0,
        upper=None,
        semantic_types=_stypes,
        description='degree for poly kernel'
    )

    gamma = hyperparams.Bounded[float](
        default=0,
        lower=-1,
        upper=None,
        semantic_types=_stypes,
        description=(
            'gamma for poly, rbf and sigmoid kernels. A value of zero'
            ' causes gamma to be computed as 1/(num_features * var(X))'
            ', any negative value causes gamma to be computed as'
            ' 1/num_features')
    )

    coef0 = hyperparams.Hyperparameter[float](
        default=0,
        description='coef0 for poly and sigmoid kernels',
        semantic_types=_stypes
    )

    epsilon = hyperparams.Hyperparameter[float](
        default=0.001,
        description='tolerance for convergence',
        semantic_types=_stypes
    )


# Common base class for SVC and SVR.
class _SVMCommon:
    # type should be 'c' for classification and 'r' for regression.
    def __init__(self, type):
        self._type = type
        self._inputs = None
        self._outputs = None
        self._fitted = False
        self._model = None
        # Output column names and types.
        self._col_names = None
        self._col_types = None
        self._classes = None

    def _set_training_data(self, *, inputs: DataFrame, outputs: DataFrame):
        self._inputs = inputs
        self._outputs = outputs
        self._fitted = False

    def _fit(self,
             *,
             timeout: float=None,
             iterations: int=None) -> CallResult:
        if self._inputs is None or self._outputs is None:
            raise ValueError('Missing training data')
        # For compatibility with sklearn, we do a categorical encoding.
        if self._type == 'c':
            y, self._classes = pl.ml.d3m.d3mDataFrame.catEncode(
                self._outputs, _nOut=2)
            y = d3m_df_to_array(y, 'i', squeeze=True)
        else:
            y = d3m_df_to_array(self._outputs, 'f', squeeze=True)

        hp = self.hyperparams
        self._model = pl.ml.svm.train(
            x=d3m_df_to_array(self._inputs, dtype='f'),
            y=y,
            kernel=hp['kernel'],
            degree=hp['degree'],
            gamma=hp['gamma'],
            coef0=hp['coef0'],
            C=hp['C'],
            tol=hp['epsilon'],
            max_iter=iterations,
            # The type flag should be set in the inherited class as "c" or
            # "r"
            type=self._type
        )
        # Grab the column name so we can produce outputs with the same name.
        self._col_names = d3m_df_get_col_names(self._outputs)
        self._fitted = True
        return CallResult(None)

    def _produce(self,
                 *,
                 inputs: DataFrame,
                 timeout: float=None,
                 iterations: int=None) -> CallResult:
        if not self._fitted:
            raise PrimitiveNotFittedError('primitive is not fitted')
        # The SVM function only takes an Array, so we have to project down
        # to a Real Array and then convert back into a DataFrame.
        x = d3m_df_to_array(inputs, dtype='f')
        pred = pl.ml.svm.predict(self._model, x)
        # Turn back into a DataFrame with column names. We should also add back
        # any metadata that is expected - TODO.
        #
        # For classification, we return the categories from the encoding, so it
        # should have the same type as the training data. For regression, it is
        # not obvious how to go from float to bool or int, so we leave as
        # float.
        pred = pl.ml.d3m.d3mDataFrame.fromArray(
            pred, columnNames=self._col_names, classes=self._classes)
        return CallResult(pred)

    def _get_params(self) -> Params:
        if self._fitted:
            return Params(model=self._model, col_names=self._col_names,
                          col_types=self._col_types, classes=self._classes)
        else:
            return Params(model=None, col_names=None, col_types=None,
                          classes=None)

    def _set_params(self, *, params: Params) -> None:
        self._model = params['model']
        self._col_names = params['col_names']
        self._col_types = params['col_types']
        self._classes = params['classes']
        self._fitted = self._model is not None


class PlurosSVC(SupervisedLearnerPrimitiveBase[DataFrame,
                                               DataFrame,
                                               Params,
                                               Hyperparams], _SVMCommon):
        """ Primitive wrapping for Pluros function
        [`ml.svm.*`](https://pluros.io/doc/pluros/functions/ml.html#ml.svm).
        """

        __author__ = 'Royal Caliber'

        metadata = PrimitiveMetadata({
            "algorithm_types": [PrimitiveAlgorithmType.SUPPORT_VECTOR_MACHINE],
            "name": "PlurosSVC",
            "primitive_family": PrimitiveFamily.CLASSIFICATION,
            "python_path": "d3m.primitives.classification.svc.Pluros",
            "source": {
                'name': 'Royal Caliber',
                'contact': 'mailto:vishal@royal-caliber.com',
                'uris': ['https://gitlab.com/royalcaliber/oss/pluros-d3m']
            },
            "version": "2021.12.13",
            "id": "0dd8a9ae-5352-4d41-acab-6d7913ab9928",
            "hyperparams_to_tune": ['C', 'kernel', 'gamma'],
            'installation': [{
                'type': PrimitiveInstallationType.PIP,
                'package': 'pluros-d3m',
                'version': '>=0.0.0',
            }]
        })

        def __init__(self,
                     *,
                     hyperparams: Hyperparams,
                     random_seed: int=0,
                     docker_containers: Dict[str, DockerContainer]=None):
            super().__init__(
                hyperparams=hyperparams,
                random_seed=random_seed,
                docker_containers=docker_containers)
            _SVMCommon.__init__(self, type='c')

        def set_training_data(self, *, inputs: DataFrame, outputs: DataFrame):
            return self._set_training_data(inputs=inputs, outputs=outputs)

        def fit(self,
                *,
                timeout: float=None,
                iterations: int=None) -> CallResult:
            return self._fit(timeout=timeout, iterations=iterations)

        def produce(self,
                    *,
                    inputs: DataFrame,
                    timeout: float=None,
                    iterations: int=None) -> CallResult:
            return self._produce(inputs=inputs, timeout=timeout,
                                 iterations=iterations)

        def get_params(self) -> Params:
            return self._get_params()

        def set_params(self, *, params: Params) -> None:
            return self._set_params(params=params)


class PlurosSVR(SupervisedLearnerPrimitiveBase[DataFrame,
                                               DataFrame,
                                               Params,
                                               Hyperparams], _SVMCommon):
        """ Primitive wrapping for Pluros function
        [`ml.svm.*`](https://pluros.io/doc/pluros/functions/ml.html#ml.svm).
        """

        __author__ = 'Royal Caliber'

        metadata = PrimitiveMetadata({
            "algorithm_types": [PrimitiveAlgorithmType.SUPPORT_VECTOR_MACHINE],
            "name": "PlurosSVR",
            "primitive_family": PrimitiveFamily.REGRESSION,
            "python_path": "d3m.primitives.regression.svr.Pluros",
            "source": {
                'name': 'Royal Caliber',
                'contact': 'mailto:vishal@royal-caliber.com',
                'uris': ['https://gitlab.com/royalcaliber/oss/pluros-d3m']
            },
            "version": "2021.12.13",
            "id": "f189d065-f555-4718-9941-9da5bedebe8b",
            "hyperparams_to_tune": ['C', 'kernel', 'gamma'],
            'installation': [{
                'type': PrimitiveInstallationType.PIP,
                'package': 'pluros-d3m',
                'version': '>=0.0.0',
            }]
        })

        def __init__(self,
                     *,
                     hyperparams: Hyperparams,
                     random_seed: int=0,
                     docker_containers: Dict[str, DockerContainer]=None):
            super().__init__(
                hyperparams=hyperparams,
                random_seed=random_seed,
                docker_containers=docker_containers)
            _SVMCommon.__init__(self, type='r')

        def set_training_data(self, *, inputs: DataFrame, outputs: DataFrame):
            return self._set_training_data(inputs=inputs, outputs=outputs)

        def fit(self,
                *,
                timeout: float=None,
                iterations: int=None) -> CallResult:
            return self._fit(timeout=timeout, iterations=iterations)

        def produce(self,
                    *,
                    inputs: DataFrame,
                    timeout: float=None,
                    iterations: int=None) -> CallResult:
            return self._produce(inputs=inputs, timeout=timeout,
                                 iterations=iterations)

        def get_params(self) -> Params:
            return self._get_params()

        def set_params(self, *, params: Params) -> None:
            return self._set_params(params=params)
