from typing import Optional, Any, Dict, List

from d3m.metadata import params, hyperparams
from d3m.metadata.base import (
    PrimitiveMetadata,
    PrimitiveAlgorithmType,
    PrimitiveFamily,
    PrimitiveInstallationType)
from d3m.container import DataFrame
from d3m.exceptions import PrimitiveNotFittedError
from d3m.primitive_interfaces.unsupervised_learning \
    import UnsupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import DockerContainer

import pluros as pl
import pluros.numpy_compat as pnp
from plurosd3m.primitives.proxy import CallResult
from plurosd3m.primitives.plprim.util import (
    d3m_df_to_array, d3m_df_get_col_names)


# Shorthand.
_stypes = ['https://metadata.datadrivendiscovery.org/types/TuningParameter']


# Params support to be added later if needed. Right now we just hold on to the
# model itself.
class Params(params.Params):
    u: Optional[Any]
    s: Optional[Any]
    v: Optional[Any]


class Hyperparams(hyperparams.Hyperparams):
    k = hyperparams.Bounded[float](
        default=2,
        lower=0,
        upper=None,
        description='Number of singular vectors to compute',
        semantic_types=_stypes
    )

    tol = hyperparams.Bounded[float](
        default=0,
        lower=0,
        upper=None,
        description='Convergence threshold, 0 means set automatically.',
        semantic_types=_stypes
    )


class PlurosTSVD(UnsupervisedLearnerPrimitiveBase[DataFrame,
                                                  DataFrame,
                                                  Params,
                                                  Hyperparams]):
    """Truncated Singular Value Decomposition.
    """
    __author__ = 'Royal Caliber'
    metadata = PrimitiveMetadata({
        "algorithm_types": [PrimitiveAlgorithmType.SINGULAR_VALUE_DECOMPOSITION],
        "name": "PlurosTSVD",
        "primitive_family": PrimitiveFamily.DATA_PREPROCESSING,
        "python_path": "d3m.primitives.feature_extraction.truncated_svd.Pluros",
        "source": {
            'name': 'Royal Caliber',
            'contact': 'mailto:vishal@royal-caliber.com',
            'uris': ['https://gitlab.com/royalcaliber/oss/pluros-d3m']
        },
        "version": "2022.01.20",
        "id": "bc4d4c90-8178-45f2-81e6-a64eb2c11e71",
        "hyperparams_to_tune": ['k'],
        'installation': [{
            'type': PrimitiveInstallationType.PIP,
            'package': 'pluros-d3m',
            'version': '>=0.0.0',
        }]
    })


    def __init__(self,
                 *,
                 hyperparams: Hyperparams,
                 random_seed: int=0,
                 docker_containers: Dict[str, DockerContainer]=None):
        super().__init__(
            hyperparams=hyperparams,
            random_seed=random_seed,
            docker_containers=docker_containers)
        self._inputs = None
        self._fitted = False
        self._u = None
        self._s = None
        self._v = None

    def set_training_data(self, *, inputs: DataFrame):
        self._inputs = inputs
        self._fitted = False

    def fit(self,
            *,
            timeout: float=None,
            iterations: int=None) -> CallResult:
        hp = self.hyperparams
        # Wrap outputs as Arrays
        self._s, self._u, self._v = pnp.asarrays(pl.array.linalg.tsvd(
            # Keep sparse tables sparse.
            d3m_df_to_array(self._inputs, dtype='f'),
            k=hp['k'],
            tol=hp['tol'],
            _nOut=3
        ))
        return CallResult(None)

    def produce(self,
                *,
                inputs: DataFrame,
                timeout: float=None,
                iterations: int=None) -> CallResult:
        # Keep sparse tables as sparse matrices.
        x = pnp.asarray(d3m_df_to_array(inputs, dtype='f'))
        out = x @ self._v
        # Convert back to a table.
        df = pl.ml.d3m.d3mDataFrame.fromArray(out)
        return CallResult(df)

    def get_params(self) -> Params:
        return Params(u=self._u, s=self._s, v=self._v)

    def set_params(self, *, params: Params) -> None:
        self._u = params['u']
        self._s = params['s']
        self._v = params['v']
        self._fitted = self._v is not None
