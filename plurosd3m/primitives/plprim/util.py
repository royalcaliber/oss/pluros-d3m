# Some helper functions.

import numpy as np
from d3m.container import DataFrame as D3MDataFrame
import pluros as pl
from pluros.numpy_compat import asarray


# Returns the Pluros type symbol corresponding to a numpy dtype.
def dtype_to_symbol(dtype):
    if not isinstance(dtype, str):
        dtype = dtype.kind
    if dtype.startswith('f'):
        return pl.symbol('Real')
    elif dtype.startswith('i'):
        return pl.symbol('Int')
    elif dtype.startswith('b'):
        return pl.symbol('Bool')
    else:
        raise ValueError(f"unsupported dtype {dtype}")


# Converts D3MDataFrame objects to numpy arrays. If the input is a local
# object, converts to a local numpy array. If the input is a Pluros Value,
# converts to a remote Array, wrapped with a numpy compatibility wrapper.
def d3m_df_to_array(x, dtype, squeeze=False):
    if isinstance(x, D3MDataFrame):
        ret = x.to_numpy(dtype=dtype)
        return np.squeeze(ret) if squeeze else ret
    else:
        return asarray(
            pl.ml.d3m.d3mDataFrame.toArray(
                x, dt=dtype_to_symbol(dtype), squeeze=squeeze))


# Retrieves the column names from a D3MDataFrame. If local, the column names
# returned are local, if remote, the column names are remote.
def d3m_df_get_col_names(df):
    if isinstance(df, D3MDataFrame):
        return list(df.columns)
    else:
        return pl.ml.d3m.d3mDataFrame.getColumnNames(df)


# Retrieves the dtypes of a D3MDataFrame object as a list of type names as
# understood by `numpy.dtype`. If local, the returned list is a local list,
# otherwise it is a remote list.
def d3m_df_get_dtypes(df):
    if isinstance(df, D3MDataFrame):
        return [x.str for x in df.dtypes]
    else:
        return pl.ml.d3m.d3mDataFrame.getDataTypes(df)
