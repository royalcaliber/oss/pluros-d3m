# This is a single generic proxy primitive that uses the pluros.ml.d3m
# functions to emulate a local primitive.
import copyreg

import d3m
import d3m.primitive_interfaces.base

import pluros as pl
import pluros.client as plc

from ..metadata import json_to_metadata


# This is a shallow Value wrapper that produces a d3m CallResult when
# pluros.get() is applied to it.
class CallResult(pl.ValueWrapper, d3m.primitive_interfaces.base.CallResult):
    """ Wraps the result of a call to a Pluros proxy primitive. Use
    get() to get a local CallResult.
    """
    def __init__(self, value):
        self.value = value
        # Repeated invocation can be easily supported, we just need to write
        # some tests. For now, disabled.
        self.has_finished = True
        # We currently ignore the iterations argument. Can enable later if it
        # is used.
        self.iterations_done = None

    def get(self):
        """ Blocks until the remote Value is ready and returns a local
        CallResult containing the result.
        """
        try:
            return d3m.primitive_interfaces.base.CallResult(
                value=self.value.get(),
                has_finished=True,
                iterations_done=False)
        except Exception as ex:
            return d3m.primitive_interfaces.base.CallResult(
                value=ex,
                has_finished=False,
                iterations_done=None)

    def eval(self):
        """ Request computation of this result without blocking.
        """
        self.value.eval()


class MultiCallResult(
        pl.ValueWrapper, d3m.primitive_interfaces.base.MultiCallResult):
    def __init__(self, values):
        """Wraps the result of a call to multi_produce of the Pluros proxy
        primitive. Use get() to convert to a local MultiCallResult.
        """
        self.values = values
        # Fake it till its actually used.
        self.has_finished = True
        # Ignored.
        self.iterations_done = None

    def get(self):
        """Blocks until all produce results have been computed and returns a
        local MultiCallResult value when done.
        """
        # Compute everything in parallel.
        self.eval()
        # Now collect each result, package exceptions appropriately.
        ret = {}
        any_failed = False
        for method, result in self.values.items():
            try:
                ret[method] = pl.get(result)
            except Exception as ex:
                ret[method] = ex
                any_failed = True
        return d3m.primitive_interfaces.base.MultiCallResult(
            values=ret, has_finished=not any_failed)

    def eval(self):
        """Request evaluation of all results.
        """
        pl.eval(self.values.values())


# Metaclass to host the metadata property on the class for compatibility
# with d3m.
#
# From python 3.9, we can simply use @property, @classmethod and @cache to
# achieve the same effect without creating this metaclass etc.
class PrimitiveMeta(type):
    @property
    def metadata(self):
        if self._metadata is None:
            self._metadata = json_to_metadata(
                pl.ml.d3m.primitiveMetadata(self.python_path).get())
        return self._metadata


# This is a cache of proxy primitive classes by path.
_proxy_class_cache = {}


# Make proxy classes (both the class and the instance) picklable using copyreg.
copyreg.pickle(PrimitiveMeta,
               lambda cls: (proxy_primitive, (cls.python_path,)))


def proxy_primitive(python_path_):
    """ Returns a proxy primitive class for a Pluros D3M primitive with the
    given python_path attribute. The returned primitive can be used much as a
    regular primitive, with the difference that the output produced by a proxy
    primitive are handles to Pluros values and an explicit .get() is required
    to fetch the value into a local object.
    """

    # Check if we have the class in the cache.
    try:
        return _proxy_class_cache[python_path_]
    except KeyError:
        # Doesn't exist, create the class below.
        pass

    # Currently only PrimitiveBase functions are implemented. The fancier
    # interfaces will be added later.
    class Primitive(metaclass=PrimitiveMeta):
        # Record the python_path
        python_path = python_path_

        # Cache the metadata for the class
        _metadata = None

        def __init__(self, *, hyperparams, **kwargs):
            """ Initializes a Pluros proxy primitive. All arguments besides
            hyperparams are ignored.
            """
            # Cache remotely, to avoid repeated serialization.
            self._hyperparams = pl.identity(hyperparams)
            self._fitted = None
            self._kwargs = kwargs

        def produce(self, *, inputs, timeout=None, iterations=None):
            """ See d3m.primitives_interfaces.PrimitiveBase.

            The `timeout` and `iterations` arguments are ignored.
            """
            if self._fitted is None:
                # Perhaps this is a transformer primitive, so we try to produce
                # without fitting.
                return CallResult(pl.ml.d3m.transform(
                    self.python_path, inputs, self._hyperparams))
            else:
                return CallResult(pl.ml.d3m.produce(self._fitted, inputs))

        def multi_produce(self, *, produce_methods, inputs, timeout=None,
                          iterations=None):
            """ See d3m.primitives_interfaces.PrimitiveBase.

            The `timeout` and `iterations` arguments are ignored.
            """
            if self._fitted is None:
                # Assume this is a proxy for a transformer. Other produce
                # methods not currently supported.
                values = {
                    method: pl.ml.d3m.transform(self.python_path,
                                                inputs,
                                                self._hyperparams,
                                                method)
                    for method in produce_methods
                }
                return MultiCallResult(values)
            else:
                values = {
                    method: pl.ml.d3m.produce(self._fitted, inputs, method)
                    for method in produce_methods
                }
            return MultiCallResult(values)

        def fit_multi_produce(self, *, produce_methods, inputs=None,
                              outputs=None, timeout=None, iterations=None):
            """ See d3m.primitives_interfaces.PrimitiveBase.

            The `timeout` and `iterations` arguments are ignored.
            """
            self.set_training_data(inputs=inputs, outputs=outputs)
            self.fit()
            return self.multi_produce(
                produce_methods=produce_methods, inputs=inputs)

        def set_training_data(self, *, inputs=None, outputs=None):
            """ See d3m.primitives_interfaces.PrimitiveBase.

            Note: Pluros function calls are asynchronous and pure. Consider
            the following sequence of calls to a proxy primitive `p`.
            ```
                p.set_training_data(x1, y1)
                p.fit()
                y2 = p.produce(x2)
                p.set_training_data(x3, y3)
                p.fit()
                y4 = p.produce(x4)
            ```
            The second call to p.set_training_data occurs before y2 has been
            computed (since there is no y2.get()). But this is ok: both y2 and
            y4 will be correctly (and concurrently) computed.
            """
            self._inputs = inputs
            self._outputs = outputs

        def fit(self, *, timeout=None, iterations=None):
            """ See d3m.primitives_interfaces.PrimitiveBase.

            The `timeout` and `iterations` arguments are ignored.
            """
            self._fitted = pl.ml.d3m.fit(
                self.python_path, self._inputs, self._outputs,
                self._hyperparams)

        def get_params(self):
            """Captures the remote state of the fitted primitive.
            """
            return {'fitted': self._fitted, 'hp': self._hyperparams}

        def set_params(self, params):
            """Sets the proxy wrapper to a previously fitted primitive's state.
            """
            self._fitted = params['fitted']
            self._hyperparams = params['hp']

    # Record this in the class cache before returning.
    _proxy_class_cache[python_path_] = Primitive
    return Primitive


def is_proxy_primitive(P):
    """ Returns True if a D3M primitive class P is a Pluros proxy primitive.
    """
    return type(P) is PrimitiveMeta
