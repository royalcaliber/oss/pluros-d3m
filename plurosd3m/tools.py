"""
Utilities for using Pluros accelerated ML primitives for D3M.
"""

import d3m.metadata.pipeline as pipeline
from d3m.metadata.base import ArgumentType
import logging
import pluros as pl
from . import primitives


class UnsupportedComputationError(Exception):
    """Exception to be raised by accelerated primitives when inputs or
    hyperparameters are not supported."""
    pass


# A 'Replacement Context' for deciding whether a primitive can be substituted
# by the accelerated version.
#
# Currently this does not include the actual inputs, but there are cases where
# that may be necessary. TODO.
class ReplContext:
    def __init__(self, ver=None, hyperparams=None):
        self.primitive_version = ver    # version of the original primitive
        self.hyperparams = hyperparams  # static hyperparameters


class MapEntry:
    """Entry recording mapping between original primitive (`id`) and its
    replacement `repl`. `repl` should be API-compatible with D3M primitives
    and can additionally define a classmethod `_pld3m_can_replace` that takes
    a ReplContext and returns a boolean value indicating if the replacement
    is acceptable for a specific set of hyperparameters.
    """
    def __init__(self, id, repl):
        """
        Arguments:
            `id`: id as specified in the original primitive's D3M metadata.
            `repl`: a replacement Primitive object.
        """
        self.id = id
        self.replacement = repl


_substitution_map = {}


def register_substitution(id, repl):
    """Add an entry to the primitive substitution table. This will be reflected
    in the next call to `optimize_pipeline`.

    Arguments:
        `id`: id as specified in the original primitive's D3M metadata.
        `repl`: a replacement Primitive object.

    Returns:
        None
    """
    logger = logging.getLogger(__name__)
    logger.info('added substitution for %s to %s' % (id, repl))
    _substitution_map[id] = MapEntry(id, repl)


_loaded_local_primitives = False


# this will be changed to load from a file later for simpler maintenance. Right
# now, it's a short hard-coded list.
def _load_local_primitives():
    global _loaded_local_primitives
    if _loaded_local_primitives:
        return

    # Import all primitives.
    import plurosd3m.primitives.sklearn
    import plurosd3m.primitives.distil

    # shorthand.
    M = MapEntry
    P = primitives
    entries = [
        M('0ae7d42d-f765-3348-a28c-57d94880aa6a', P.sklearn.SKSVC),
        M('9231fde3-7322-3c41-b4cf-d00a93558c44', P.sklearn.SKTruncatedSVD),
        M('3b09024e-a83b-418c-8ff4-cf3d30a9609e', P.distil.KMeansPrimitive),
        M('ebbc3404-902d-33cc-a10c-e42b06dfe60c', P.sklearn.SKSVR),
        M('01d2c086-91bf-3ca5-b023-5139cf239c77', P.sklearn.SKGradientBoostingClassifier),  # noqa
        M('2a031907-6b2c-3390-b365-921f89c8816a', P.sklearn.SKGradientBoostingRegressor),   # noqa
        M('1dd82833-5692-39cb-84fb-2455683075f3', P.sklearn.SKRandomForestClassifier),  # noqa
        M('f0fd7a62-09b5-3abc-93bb-f5f999f7cc80', P.sklearn.SKRandomForestRegressor),   # noqa
        M('e20d003d-6a9f-35b0-b4b5-20e42b30282a', P.sklearn.SKDecisionTreeClassifier),  # noqa
        M('6c420bd8-01d1-321f-9a35-afc4b758a5c6', P.sklearn.SKDecisionTreeRegressor),   # noqa
        # MapEntry('2fb16403-8509-3f02-bdbf-9696e2fcad55', pl_SKRidge),
    ]
    for entry in entries:
        _substitution_map[entry.id] = entry

    _loaded_local_primitives = True


# Helper for optimize_pipeline.
def _can_replace(entry, repl_context):
    if not entry:
        return False
    if not hasattr(entry.replacement, '_pld3m_can_replace'):
        return True
    return entry.replacement._pld3m_can_replace(repl_context)


# Helper for optimize_pipeline.
#
# Gets the hyperparameters specified for step in the pipeline.
#
# This is modeled after Runtime._get_pipeline_hyperparams.
def _get_pipeline_hyperparams(step):
    ret = {}
    for name, desc in step.hyperparams.items():
        if desc['type'] == ArgumentType.VALUE:
            ret[name] = desc['data']
        else:
            # Other types of hyperparams not yet implemented. Will implement
            # when we have an accelerated primitive that takes these types of
            # hyperparams.
            raise NotImplementedError('unsupported hyperparam type')
    return ret


# There are three ways to work with optimize_pipeline:
#
# Method 1:
#   Call optimize_pipeline() outside the hyperparameter tuning loop. This will
# switch in accelerated primitives where available. The accelerated primitives
# may advertise a slightly different hyperparameter space than the original
# primitive. As long as the tuning code can handle this transparently, this
# is fine.
#
# Method 2:
#   Call optimize_pipeline() with a specific set of hyperparameters. This will
# only substitute accelerated primitives that support the chosen set of
# hyperparameters. This can be used when the TA2 code does not want to alter
# the space of available hyperparameters and only use optimizations when
# available.

def optimize_pipeline(ppln, hyperparams=None, logger=None):
    """
    Modify the pipeline `p` for accelerated execution using Pluros. Any
    primitives whose ids and versions match the current substitution table are
    replaced with accelerated primitives.

    Arguments:
        ppln: A D3M pipeline
        hyperparams: Hyperparameter values for each step of the pipeline, in
          the same format (e.g. list of dictionaries) that d3m.runtime.Runtime
          takes.
        logger: Python logger to log to.

    Returns:
        True if any changes were made to the pipeline. False if no changes
        were made.

    Note:
      The changes made to the pipeline can be undone with
      undo_optimize_pipeline.
    """
    _load_local_primitives()
    changed_something = False
    for iStep, step in enumerate(ppln.steps):
        if isinstance(step, pipeline.PrimitiveStep):
            hp = _get_pipeline_hyperparams(step)
            if hyperparams:
                hp = hp.replace(hyperparams[iStep])
            repl = get_optimized_primitive(step.primitive, hp)
            if repl is not None:
                step._pluros_original = step.primitive
                step.primitive = repl
                changed_something = True
        elif isinstance(step, pipeline.SubpipelineStep):
            optimize_pipeline(step)
        else:
            # what are we supposed to do with placeholders?
            pass
    return changed_something


# Undoes the effects of optimize_pipeline, replacing any accelerated primitives
# with the original ones. Used by the runtime when the accelerated version
# fails for any reason.
def undo_optimize_pipeline(ppln):
    for step in ppln.steps:
        if hasattr(step, '_pluros_original'):
            step.primitive = step._pluros_original


def pipeline_resolver(resolver):
    """
    Wraps the given D3M pipeline resolver `resolver` as a callable that returns
    a pipeline with Pluros accelerated primitives.
    """
    def wrapped(*args, **kwargs):
        p = resolver(*args, **kwargs)
        optimize_pipeline(p, logger=kwargs.get('logger'))
        return p
    return wrapped


# Factor this with optimize_pipeline:  TODO.
def get_optimized_primitive(primitive, hyperparams=None):
    """
    If the primitive `primitive` with the hyperparameters `hyperparams` can be
    replaced by an optimized primitive, returns the optimized primitive, else
    returns None.
    """
    _load_local_primitives()
    md = primitive.metadata.query()
    id = md['id']
    ver = md['version']
    name = md['name']
    entry = _substitution_map.get(id)
    if entry is None:
        return
    if hyperparams is None:
        return entry.replacement
    def_hp = primitive.metadata.get_hyperparams().defaults()
    hp = def_hp.replace(hyperparams)
    if _can_replace(entry, ReplContext(ver, hp)):
        return entry.replacement
    return None


def is_optimizable(primitive):
    """Returns whether the given primitive is potentially optimizable for some
    set of hyperparameters. Note that for a specific set of hyperparameters
    no optimizations may be possible even if this returns True.
    """
    _load_local_primitives()
    return primitive.metadata.query()['id'] in _substitution_map


# Used as cache for _have_primitives_remote/_have_primitives_local These can be
# cleared with _invalidate_primitives_cache().
_remote_pluros_primitives = None
_local_pluros_primitives = None


# Invalidates the cache of local and remote primitives.
def _invalidate_primitives_cache():
    global _remote_pluros_primitives
    global _local_pluros_primitives
    _remote_pluros_primitives = None
    _local_pluros_primitives = None


def get_pluros_primitive(python_path, local=True):
    """Given the Python path for a primitive in `python_path`, if there is an
    accelerated version of the same primitive available within PlurosD3M,
    returns the Python path for that primitive, otherwise returns None. The
    `local` argument decides where we look for primitives: if `local` is True,
    the local collection of primitives is searched. If `local` is False, then
    the primitives offered by the Pluros service is looked up. Primitives on
    the service can be accessed using the plurosd3m.primitives.proxy module.

    Note that while the returned primitive implements the same machine learning
    technique, the primitive may differ in terms of specific hyperparameters
    etc. and appropriate information should be obtained programmatically from
    the metadata.
    """
    # Replace the last component of the path with .Pluros and see if we have
    # one.
    prefix, suffix = python_path.rsplit('.', 1)
    ret = prefix + '.Pluros'
    if local and _have_primitive_local(ret):
        return ret
    elif _have_primitive_remote(ret):
        return ret
    else:
        return None


# Returns the python path for a faster alternative to `python_path` that is
# available within this module.
def _have_primitive_local(python_path):
    global _local_pluros_primitives
    if _local_pluros_primitives is None:
        import d3m.index
        _local_pluros_primitives = set(d3m.index.search())
    return python_path in _local_pluros_primitives


# Returns the python path of a faster alternative to `python_path` that is
# available on the service.
def _have_primitive_remote(python_path):
    global _remote_pluros_primitives
    # Get list of remote primitives if haven't cached already.
    if _remote_pluros_primitives is None:
        _remote_pluros_primitives = set(pl.ml.d3m.listPrimitives().get())
    return python_path in _remote_pluros_primitives
