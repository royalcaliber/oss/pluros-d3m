#!/usr/bin/env python

import os.path

import setuptools


# Location of this file during installation.
setupdir = os.path.dirname(__file__)


# Returns the given path relative to the setupdir
def reldir(pth):
    return os.path.join(setupdir, pth)


# Get the version of the package by executing the version.py file.
def _get_version():
    pth = reldir('plurosd3m/version.py')
    glbls = {}
    with open(pth, 'rt') as f:
        exec(f.read(), glbls)
    return glbls['VERSION']


setuptools.setup(name = 'pluros-d3m',
    version = _get_version(),
    author = 'Royal Caliber LLC',
    author_email = 'vishal@royal-caliber.com',
    description = 'Pluros accelerated ML primitives for D3M',
    url = 'https://pluros.io',
    packages = setuptools.find_packages(),
    python_requires = '>=3',
    classifiers = [
        "Development Status :: 2 - Pre-Alpha",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Scientific/Engineering",
        "Topic :: System :: Clustering",
        "Topic :: System :: Distributed Computing",
        "Topic :: Software Development :: Libraries"
    ],
    zip_safe = False,
    install_requires = ['pluros-python-client[compat]', 'd3m'],
    # Register primitives with D3M index.
    entry_points = {
      'd3m.primitives': [
        'classification.svc.Pluros = plurosd3m.primitives.plprim:PlurosSVC',
        'regression.svr.Pluros = plurosd3m.primitives.plprim:PlurosSVR',
        'classification.random_forest.Pluros = plurosd3m.primitives.plprim:PlurosRFC',
        'regression.random_forest.Pluros = plurosd3m.primitives.plprim:PlurosRFR',
        'clustering.k_means.Pluros = plurosd3m.primitives.plprim:PlurosKMeans',
        'feature_extraction.truncated_svd.Pluros = plurosd3m.primitives.plprim:PlurosTSVD',
        'data_transformation.adjacency_spectral_embedding.Pluros = plurosd3m.primitives.plprim:PlurosASE'
      ]
    }
)
