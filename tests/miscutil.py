# Some shared utilities for the tests.

import sklearn.metrics
import pluros as pl
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
import d3m.runtime
from d3m.container.pandas import DataFrame

import mkpipeline


def get_id(p):
    return p.metadata.query()['id']


def get_ppln(pplnjs):
    """Returns a Pipeline from a json description.
    """
    return Pipeline.from_json_structure(pplnjs)


def get_hyperparams(prim, **kwargs):
    """Return a Hyperparams object for a primitive, modified by the given
    keywords.
    """
    hp = prim.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults().copy()
    hp.update(**kwargs)
    return hp


def set_hp(ppln, id, **hp):
    """Sets the given hyperparameters on the primitive with the given
    id in the given pipeline.
    """
    for step in ppln.steps:
        if isinstance(step, PrimitiveStep):
            if step.primitive.metadata.query()['id'] == id:
                for k, v in hp.items():
                    step.add_hyperparameter(
                        name=k, data=v, argument_type=ArgumentType.VALUE)


def run_ppln(ppln, dataset, truth=None, metric=None):
    """Runs and scores a pipeline on a dataset.
    """
    r = d3m.runtime.Runtime(pipeline=ppln, context='TESTING')
    result = r.fit([dataset])
    if result.has_error():
        raise result.error
    if truth is not None:
        assert metric is not None
        return metric(truth, pl.get(result.values['outputs.0']))
    else:
        return result.values['outputs.0']


def array_to_df(x):
    return DataFrame({'col%d' % i: x[:, i] for i in range(x.shape[1])})


class TestBase:
    # Runs a regression or classification pipeline. type should be "c" or "r".
    def run_ppln(self, ppln, dataset, truth, type):
        if type == 'c':
            metric = sklearn.metrics.f1_score
        else:
            metric = sklearn.metrics.mean_squared_error
        return run_ppln(ppln, dataset, truth, metric)

    # Compares the scores from each pipeline in pplns. Hyperparameters can be
    # specified through kwargs.
    def run_test(self, pplns, dataset, truth, type, rtol=0.05):
        # Get all the scores.
        scores = [self.run_ppln(ppln, dataset, truth, type) for ppln in pplns]
        print(scores)
        # Check that they are all within range. The first is assumed to be the
        # reference.
        ref = scores[0]
        for score in scores[1:]:
            if type == 'c':
                self.assertGreaterEqual(score, ref * (1 - rtol))
            else:
                self.assertLessEqual(score, ref * (1 + rtol))

    def mk_pplns(self, scaled, type, prims, **kwargs):
        pplns = [
            mkpipeline.gen_cr_pipeline(prim, type=type, scaled=scaled, hp=hp)
            for prim, hp in prims.items()
        ]
        return pplns
