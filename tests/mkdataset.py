# Utilities to generate datasets for unit testing and benchmarking

import d3m
import d3m.container
from d3m.metadata.base import ALL_ELEMENTS
import pandas
import numpy as np
from collections import OrderedDict as ODict


# shorthand
SEMTYPE_PRIMARY_KEY = 'https://metadata.datadrivendiscovery.org/types/PrimaryKey'  # noqa
SEMTYPE_TRUE_TARGET = 'https://metadata.datadrivendiscovery.org/types/TrueTarget'  # noqa
SEMTYPE_ATTRIBUTE = 'https://metadata.datadrivendiscovery.org/types/Attribute'     # noqa


def set_semantic_type(ds, col, sem):
    """Add the semantic type `sem` to column index `col` of the 'learningData'
    resource of the dataset `ds`.
    """
    ds.metadata = ds.metadata.add_semantic_type(
        ('learningData', ALL_ELEMENTS, col),
        sem)


def gen_column(colcode, size):
    """Generate a feature column.

    If coltype is:
        'f': returns a normal random variable with randomly chosen mu and
             sigma.
        'i': returns uniform integers in a random range.
        's': returns random sequences of strings from a random subset of
             alphabet.
        'b': returns a bernoulli variable with randomly chosen p.
        list: returns a sample from a random subset of the list.
    """
    from numpy.random import uniform, normal, choice, randint
    if colcode == 'f':
        mu = uniform(-1, 1)
        sigma = uniform(0, 10000)
        return normal(mu, sigma, size=size)
    elif colcode == 'i':
        a, b = np.sort(randint(0, 10000, size=2))
        return randint(a, b, size=size)
    elif colcode == 's':
        subset_size = randint(2, len(alphabet))
        subset = choice(alphabet, size=subset_size, replace=False)
        return [choice(subset, size=randint(3, 30)) for i in range(size)]
    elif colcode == 'b':
        p = uniform(0.2, 0.8)
        return binomial(1, p, size=size)
    else:  # colcode should be a list of enumerated values.
        subset = choice(colcode, randint(1, len(colcode)), replace=False)
        return choice(subset, size=size)


def d3mdf_to_d3mds(df):
    """Converts a D3M dataframe to a D3M dataset.
    """
    cols = len(df.columns)
    ds = d3m.container.Dataset(resources={'learningData': df})
    set_semantic_type(ds, 0, SEMTYPE_PRIMARY_KEY)
    for cIdx in range(1, cols - 1):
        ds.metadata = ds.metadata.add_semantic_type(
            ('learningData', ALL_ELEMENTS, cIdx),
            SEMTYPE_ATTRIBUTE)
    ds.metadata = ds.metadata.add_semantic_type(
        ('learningData', ALL_ELEMENTS, cols - 1),
        SEMTYPE_TRUE_TARGET)
    ds.metadata = ds.metadata.update((), {'id': 'test', 'digest': '0'})
    ds.metadata = ds.metadata.update(
        ('learningData',),
        {'structural_type': d3m.container.DataFrame})
    ds.metadata = ds.metadata.update(
        ('learningData', ALL_ELEMENTS),
        {
            'dimension': {
                'name': 'columns',
                'length': cols,
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn']  # noqa
            }
        })
    ds['learningData'].metadata.generate(df)
    return ds


def df_to_d3m(tab_):
    """Converts a dictionary of column values into a D3M dataset.
    """
    cols = len(tab_)
    rows = tab_[next(iter(tab_))].shape[0]
    tab = pandas.DataFrame()
    # Add d3mIndex column as first column
    tab['d3mIndex'] = np.arange(rows)
    for c, v in tab_.items():
        tab[c] = v
    df = d3m.container.DataFrame(tab, generate_metadata=True)
    return d3mdf_to_d3mds(df)


def binclass_dataset(rows, cols, singlePrecision=False):
    """Generate a single-table binary classification dataset.

    Arguments:
        rows: number of samples
        cols: sequence of feature types, see gen_column.

    Output:
        A D3M Dataset.
    """
    # Decide number of positive samples
    nPositive = int(np.random.uniform(0.2, 0.8) * rows)
    if nPositive < 1:
        nPositive = 1
    nNegative = rows - nPositive
    assert nNegative > 0
    tab = ODict()
    for i, code in enumerate(cols):
        tab['col%d' % i] = np.concatenate(
            [gen_column(code, nPositive), gen_column(code, nNegative)])
    tab['class'] = np.arange(rows) < nPositive
    # shuffle things up
    idx = np.arange(rows)
    np.random.shuffle(idx)
    for col, data in tab.items():
        if singlePrecision and data.dtype.kind == 'f':
            tab[col] = data[idx].astype('f4')
        else:
            tab[col] = data[idx]
    return df_to_d3m(tab)


def make_simple_table(rows, cols):
    """Generate a single-table, single-target dataset for classification.

    Arguments:
        rows: the number of samples.
        cols: a string of letters indicating a type for each column:
                'f': float,
                'i': integer,
                's': string,
                'b': boolean.
              The final column is the target.

    Output:
        A D3M Dataset object.

    Only minimal metadata is assigned, namely the target is tagged as
    'TrueTarget' and the columns are tagged as 'Attributes'.

    """
    def gencol(c):
        if c == 'f':
            mu = np.random.uniform(-10000, 10000)
            sigma = np.random.uniform(0, 10000)
            return np.random.normal(mu, sigma,  size=rows)
        elif c == 'i':
            return np.random.uniform(-10000, 10000, size=rows)
        elif c == 's':
            return np.random.choice('abcdefghijklmnopqrstuvwxyz', size=rows)
        elif c == 'b':
            p = np.random.uniform(0.2, 0.8)
            return np.random.binomial(1, p, size=rows)
        else:
            raise ValueError('invalid column type "%s"' % c)
    tab = pandas.DataFrame()
    tab['d3mIndex'] = np.arange(rows)
    for i, c in enumerate(cols):
        tab['col%d' % i] = gencol(c)
    df = d3m.container.DataFrame(tab, generate_metadata=True)
    ds = d3m.container.Dataset(resources={'learningData': df})
    set_semantic_type(ds, 0, SEMTYPE_PRIMARY_KEY)
    for cIdx in range(1, len(cols)):
        ds.metadata = ds.metadata.add_semantic_type(
            ('learningData', ALL_ELEMENTS, cIdx),
            SEMTYPE_ATTRIBUTE)
    ds.metadata = ds.metadata.add_semantic_type(
        ('learningData', ALL_ELEMENTS, len(cols)),
        SEMTYPE_TRUE_TARGET)
    ds.metadata = ds.metadata.update(
        (), {'id': 'test', 'digest': '0', 'dimension': 10})
    ds.metadata = ds.metadata.update(
        ('learningData',),
        {
            'structural_type': d3m.container.DataFrame,
            'my_custom_field': 101
        })
    ds.metadata = ds.metadata.update(
        ('learningData', ALL_ELEMENTS),
        {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],  # noqa
                'length': 1 + len(cols)
            }
        })
    return ds


# Generate an edge list for graph prims. n is the number of nodes and e is the
# number of edges. Returns a d3m dataframe.
def gen_edgelist(n, e, directed=False):
    import networkx as nx
    g = nx.generators.random_graphs.gnm_random_graph(n, e, directed=directed)
    return d3m.container.DataFrame(g.edges(data=True))


if __name__ == '__main__':
    ds = binclass_dataset(100, ['f', 'f', 'f'])
    print(ds['learningData'])
