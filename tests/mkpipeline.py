# This module contains a number of functions for generating pipelines with
# primitives for which we have Pluros accelerated versions. These pipelines can
# be used both for short unit tests as well as for benchmarks.
#
# The requisite original primitives should be loadable. Consequently it may be
# necessary to run these functions within a docker container that has all the
# primitives installed.


import json
import sys
from frozendict import frozendict
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from d3m.metadata.base import Context, ArgumentType
from d3m.primitives.data_transformation import dataset_to_dataframe, \
    column_parser, extract_columns_by_semantic_types
from d3m.primitives.data_preprocessing import standard_scaler
import d3m.primitives.data_cleaning.imputer as imputer
from d3m.primitives.data_preprocessing.truncated_svd import SKlearn as SKTSVD
from d3m.primitives.clustering.k_means import DistilKMeans
from d3m.primitives.evaluation.compute_scores import Core as compute_scores


class DRef:
    """Encapsulates a data reference.
    """
    def __init__(self, type=ArgumentType.CONTAINER, group='steps', count=0,
                 name=''):
        """Wraps a data reference.
        """
        self.type = type
        self.group = group
        self.count = count
        self.name = name

    def ref(self):
        """Returns the data reference.
        """
        if self.name:
            return '%s.%d.%s' % (self.group, self.count, self.name)
        else:
            return '%s.%d' % (self.group, self.count)


class PPLN(Pipeline):
    """A convenience wrapper around the D3M Pipeline class.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_input(name='inputs')
        self._step_count = 0

    def apply(self, p, inputs=frozendict(), hp=frozendict(),
              outputs=('produce',)):
        """Applies a primitive p to the given keyword arguments and returns a
        handle to the application node. inputs should be dictionary with
        primitive argument names as key and DRefs or Data as values. outputs
        should be a list of output names.
        """
        step = PrimitiveStep(primitive_description=p.metadata.query())
        # Force the specific primitive class on the step, because of our use of
        # aliased Primitives.
        step.primitive = p

        def marshal_args(k, v):
            args = {'name': k}
            if isinstance(v, DRef):
                args['data_reference'] = v.ref()
                args['argument_type'] = v.type
            else:
                args['data'] = v
                args['argument_type'] = ArgumentType.VALUE
            return args

        for k, v in inputs.items():
            step.add_argument(**marshal_args(k, v))

        for k, v in hp.items():
            step.add_hyperparameter(**marshal_args(k, v))

        ret = []
        for output in outputs:
            step.add_output(output)
            ret.append(DRef(count=self._step_count, name=output))
        self.add_step(step)
        self._step_count += 1
        if len(ret) == 1:
            return ret[0]
        else:
            return ret


def gen_scoring_pipeline():
    """Generates a standard scoring pipeline.
    """
    p = PPLN()
    sf = p.apply(compute_scores, inputs={
        'inputs': DRef(group='inputs'),
        'score_dataset': DRef(group='inputs')
    })
    p.add_output(name='scores', data_reference=sf.ref())
    return p


def gen_cr_pipeline(prim, *, scaled=False, impute=False, score=None, type='c',
                    hp=None):
    """Generate a classification or regression pipeline using the primitive
    prim.
    """
    p = PPLN()
    df = p.apply(dataset_to_dataframe.Common,
                 inputs={'inputs': DRef(group='inputs')})
    attrs = p.apply(
        extract_columns_by_semantic_types.Common,
        inputs={'inputs': df},
        hp={'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Attribute']}  # noqa
    )
    targets = p.apply(
        extract_columns_by_semantic_types.Common,
        inputs={'inputs': df},
        hp={'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TrueTarget']}  # noqa
    )

    if impute:
        imputed_attrs = p.apply(imputer.SKlearn, inputs={'inputs': attrs})
    else:
        imputed_attrs = attrs

    if scaled:
        scaled_attrs = p.apply(standard_scaler.SKlearn,
                               inputs={'inputs': imputed_attrs})
        out = p.apply(prim,
                      inputs={'inputs': scaled_attrs, 'outputs': targets})
    else:
        out = p.apply(prim,
                      inputs={'inputs': imputed_attrs, 'outputs': targets})
    if hp:
        set_hp(p.steps[-1], hp)

    if score is not None:
        # The following does not work for adding a scoring primitive because
        # out does not have a d3mIndex column. How is that supposed to be added
        # back or preserved through the pipeline?
        #
        #  p.apply(
        #      compute_scores,
        #      inputs={'inputs': out, 'score_dataset': DRef(group='inputs')},
        #      hp={'metrics': [ElementsHyperparams(metric=score,
        #                                          k=None, pos_label=None)]})
        #  p.add_output(name='score', data_reference=out.ref())
        raise NotImplementedError('sorry not implemented yet')

    p.add_output(name='predictions', data_reference=out.ref())
    return p


def gen_tsvd_pipeline(prim, hp):
    """Generate a pipeline for the SKTruncatedSVD primitive.

    Arguments:
        k: the number of singular values to keep.
    """
    p = PPLN()
    df = p.apply(dataset_to_dataframe.Common,
                 inputs={'inputs': DRef(group='inputs')})
    attrs = p.apply(
        extract_columns_by_semantic_types.Common,
        inputs={'inputs': df},
        hp={'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Attribute']}  # noqa
    )
    out = p.apply(prim, inputs={'inputs': attrs}, hp=hp)
    p.add_output(name='output', data_reference=out.ref())
    return p


def gen_distil_kmeans_pipeline(k):
    """Generate a pipeline for the KMeans primitive.

    We feed it a DataFrame directly because the dataset_to_dataFrame
    primitive seems to strip out structural type information from the
    metadata.

    Arguments:
        k: the number of clusters to find.
    """
    p = PPLN()
#    df = p.apply(dataset_to_dataframe.Common,
#                 inputs={'inputs': DRef(group='inputs')})
#    attrs = p.apply(
#        extract_columns_by_semantic_types.Common,
#        inputs={'inputs': df},
#        hp={'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Attribute']}  # noqa
#    )
    out = p.apply(DistilKMeans, inputs={'inputs': DRef(group='inputs')},
                  hp={'n_clusters': k})
    p.add_output(name='output', data_reference=out.ref())
    return p


def gen_clustering_pipeline(prim, hp):
    """Generate a pipeline for a clustering primitive, with hyperparams hp.
    """
    p = PPLN()
    out = p.apply(
        prim,
        inputs={'inputs': DRef(group='inputs')},
        hp=hp)
    p.add_output(name='output', data_reference=out.ref())
    return p


# Set hyperparameters on a specific primitive in a pipeline.
def set_hp(step, hp):
    for k, v in hp.items():
        step.add_hyperparameter(
            name=k, data=v, argument_type=ArgumentType.VALUE)


# Generate a Pipeline for testing graph embedding primitives. This avoids the
# data loading primitive since nx can be quite slow.
def gen_graph_embedding_pipeline(prim, hp):
    p = PPLN()
    out = p.apply(
        prim,
        inputs={'inputs': DRef(group='inputs')},
        hp = hp)
    p.add_output(name='output', data_reference=out.ref())
    return p
