#!/usr/bin/env python

# Test de/serialization of D3M containers.

import os
import unittest
import logging

import pandas.testing
from d3m.container.dataset import D3MDatasetLoader as DSLoader
from d3m.container.pandas import DataFrame as DF
import pluros as pl

import plurosd3m.containers
from plurosd3m.primitives.proxy import proxy_primitive


class TestDataFrame(unittest.TestCase):
    def test0(self):
        df = DF({
            'c0': [1, 2, 3, 4],
            'c1': [0.1, 0.2, 0.3, 0.4],
            'c2': ['a', 'b', 'c', 'd']
        }, generate_metadata=True)
        # Send up the dataframe.
        pdf = pl.identity(df)
        # Retrieve it
        df1 = pdf.get()
        # Compare the table part.
        pandas.testing.assert_frame_equal(df, df1)
        # Compare the metadata part.
        md = df.metadata.to_internal_simple_structure()
        md1 = df1.metadata.to_internal_simple_structure()
        self.assertEqual(md, md1)


class TestDataset(unittest.TestCase):
    def test0(self):
        # Load a simple dataset into local memory
        nm = '185_baseball_MIN_METADATA'
        path = os.path.abspath(f'./testdata/{nm}/{nm}_dataset/datasetDoc.json')
        uri = f'file://{path}'
        ds = DSLoader().load(uri)
        # Use a primitive to extract a dataframe.
        P = proxy_primitive(
            "d3m.primitives.data_transformation.dataset_to_dataframe.Common")
        p = P(hyperparams={'dataframe_resource': 'learningData'})
        ret = p.produce(inputs=ds).get()
        if isinstance(ret.value, Exception):
            raise ret.value


if __name__ == '__main__':
    unittest.main()
