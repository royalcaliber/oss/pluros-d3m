#!/usr/bin/env python3

# Test accelerated wrapper for distil kmeans (which is a wrapper for sklearn
# kmeans).

import unittest
import mkpipeline
import mkdataset
import d3m.runtime
import d3m.cli
from d3m.metadata.base import ArgumentType
from d3m.metadata.problem import PerformanceMetric
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from d3m.primitives.clustering.k_means import DistilKMeans
from plurosd3m.tools import optimize_pipeline
from plurosd3m.primitives.plprim.kmeans import PlurosKMeans
from plurosd3m.primitives.distil.k_means import KMeansPrimitive as OldKMeans
from plurosd3m.primitives.proxy import proxy_primitive
import numpy as np
import sklearn
import sklearn.metrics as skmetrics
import copy
from math import ceil
from collections import OrderedDict
from d3m.container.pandas import DataFrame

from miscutil import *

# Proxy primitive for the Distil version running remotely.
ProxyDistilKMeans = proxy_primitive(
    "d3m.primitives.clustering.k_means.DistilKMeans")


# Generate a point cloud with well-defined clusters.
#
# k is the number of clusters.
# d is the dimension.
# n is the (approximate) number of samples.
#
# Returns the coordinates as well as the true labels.
#
# Note: copied from rapids-progs/test_kmeans.py.
def genData(n, d, k, dtype):
    nPerCluster = int(ceil(n / k))
    xs = [np.random.normal(i, 0.1, size=(nPerCluster, d)) for i in range(k)]
    labels = [np.ones(nPerCluster, dtype='i4') * i for i in range(k)]
    return np.asarray(np.vstack(xs), dtype), np.hstack(labels)


# Generate a D3MDataFrame for testing.
def genDataFrame(n, d, k, dtype='f8'):
    x, truth = genData(n, d, k, dtype)
    # convert to a table
    tab = {'col%d' % i: x[:, i] for i in range(d)}
    return DataFrame(tab), truth


# Generate a D3M data set for testing.
def genDataset(n, d, k, dtype='f8'):
    x, truth = genData(n, d, k, dtype)
    # convert to a table
    tab = {'col%d' % i: x[:, i] for i in range(d)}
    ds = mkdataset.df_to_d3m(tab)
    return ds, truth


class TestDistilKMeans(unittest.TestCase):
    # Factor this - TODO.
    #
    # Run pipeline p on dataset ds
    def runit(self, p, ds):
        r = d3m.runtime.Runtime(pipeline=p, context='TESTING')
        result = r.fit([ds])
        if result.error:
            raise result.error
        result = r.produce([ds])
        if result.error:
            raise result.error
        ret = result.values
        return ret['outputs.0']

    def test_new(self):
        ds, truth = genDataFrame(100, 3, 4)
        prims = OrderedDict([
            (DistilKMeans, {}),
            (PlurosKMeans, {}),
            (OldKMeans, {}),
            (ProxyDistilKMeans, {})
        ])
        pplns = [
            mkpipeline.gen_clustering_pipeline(prim, hp)
                for prim, hp in prims.items()
        ]
        # This just ensures everything runs. We should check that cluster
        # assignments are approximately the same. TODO.
        out = pl.get(*(self.runit(p, ds) for p in pplns))
        pass

    def test(self):
        nRows = 100
        nCols = 3
        k = 4
        x, truth = genData(nRows, nCols, k, 'f8')
        # convert to a table
        tab = {'col%d' % i: x[:, i] for i in range(nCols)}
        ds = mkdataset.df_to_d3m(tab)
        p = mkpipeline.gen_distil_kmeans_pipeline(k)

        # Run unoptimized
        ref = self.runit(p, ds['learningData'])

        # Run optimized
        assert optimize_pipeline(p)
        acc = self.runit(p, ds['learningData'])

        # Not actually testing anything here!
        pass


if __name__ == '__main__':
    unittest.main()
