# Test JHU ASE primitive

import unittest
import mkpipeline
import mkdataset
import d3m.runtime
import d3m.cli
from d3m.metadata.base import ArgumentType
from d3m.metadata.problem import PerformanceMetric
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from plurosd3m.tools import optimize_pipeline
from plurosd3m.primitives.proxy import proxy_primitive
import numpy as np
import sklearn
import sklearn.metrics as skmetrics
import copy
from collections import OrderedDict
from d3m.container import List
from d3m.container.pandas import DataFrame
from d3m.primitives.data_transformation.adjacency_spectral_embedding import JHU as JHUASE
from plurosd3m.primitives.plprim.jhu import PlurosASE

from scipy.stats import ortho_group
import miscutil
import pluros as pl


class TestASE(unittest.TestCase):
    def test_ase(self):
#        df = mkdataset.gen_edgelist(n=100, e=100)
#        ds = mkdataset.d3mdf_to_d3mds(df)
        p = mkpipeline.gen_graph_embedding_pipeline(JHUASE, {})
        import networkx as nx
        nRows = 100
        ds = mkdataset.make_simple_table(nRows, 'ff')
        df = ds['learningData']
        print("Making random graph")
        g = nx.generators.random_graphs.gnm_random_graph(
            nRows, 10 * nRows, directed=False)
        print("Made random graph")    
#        task = "vertexClassification"
        task = "linkPrediction"
        df.rename(columns={'col0': 'linkExists'}, inplace=True)
        miscutil.run_ppln(p, List([ds['learningData'], [g], None, task]))
        

if __name__ == '__main__':
    unittest.main()
