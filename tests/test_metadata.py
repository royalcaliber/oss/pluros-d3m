#!/usr/bin/env python3

# Test metadata de/serialization.

import math
import unittest
import typing

import d3m.index
from d3m.metadata.base import (
    PrimitiveMetadata,
    DataMetadata,
    PrimitiveFamily,
    PrimitiveAlgorithmType,
    PrimitiveInstallationType,
    ALL_ELEMENTS)
import d3m.metadata.hyperparams as hp

import pluros as pl
import plurosd3m.metadata as metadata


class TestJSONCoding(unittest.TestCase):
    def round_trip(self, s):
        return metadata.from_reversible_json_structure(
            metadata.to_reversible_json_structure(s))

    def test_int(self):
        self.assertTrue(self.round_trip(int) is int)

    def test_float(self):
        md0 = (1.5, float('inf'), float('-inf'))
        md1 = self.round_trip(md0)
        self.assertEqual(md0, md1)
        self.assertTrue(math.isnan(self.round_trip(float('nan'))))

    def test_str(self):
        self.assertEqual(self.round_trip('hello'), 'hello')

    def test_list(self):
        lst = (1, 2, 'hello', 1.5)
        self.assertEqual(self.round_trip(lst), lst)

    def test_typing_union(self):
        lst = (typing.Union, typing.Union[int, float])
        self.assertEqual(self.round_trip(lst), lst)

    def test_typing_any(self):
        self.assertEqual(self.round_trip(typing.Any), typing.Any)


class TestPrimitiveMetadata(unittest.TestCase):
    def setUp(self):
        # Make up some fictitious primitive metadata, that looks real enough to
        # validate.
        class Hyperparams(hp.Hyperparams):
            a = hp.Bounded[float](default=0.5, lower=0, upper=1)
            b = hp.Choice(default='foo', choices={
                'foo': hp.Hyperparams.define({
                    'x': hp.Bounded[int](default=1, lower=0, upper=3),
                    'y': hp.Constant(default=0)
                 }),
                'bar': hp.Hyperparams.define({
                    'w': hp.Bounded[float](default=0, lower=0, upper=1)
                })
            })
            c = hp.UniformBool(default=False)
            d = hp.Constant(default=float('inf'))
            e = hp.Union(default='first', configuration={
                'first': hp.Bounded[float](default=0.1, lower=0, upper=1),
                'second': hp.Constant(default='xyz')
            })
            f = hp.Enumeration(('first', 'second', 'third'), default='first')
            g = hp.Set(hp.Bounded[float](default=0.5, lower=0, upper=1),
                       default=(0, 0.1, 0.2, 0.3))
            h = hp.List(hp.Bounded[float](default=0.5, lower=0, upper=1),
                        default=(0, 0.1, 0.2, 0.3))

        hpval = {
            'type': str,
            'default': None,
            'structural_type': float,
            'semantic_types': []
        }

        self.pmd = PrimitiveMetadata({
            'algorithm_types': [PrimitiveAlgorithmType.BINARY_CLASSIFICATION],
            'id': 'abcd',
            'name': 'test_primitive',
            'primitive_family': PrimitiveFamily.CLASSIFICATION,
            'python_path': 'd3m.primitives.classification.svc.Path',
            'original_python_path': 'something.path.primitive',
            'primitive_code': {
                'class_type_arguments': {'a': int},
                'interfaces_version': 'master',
                'interfaces': ['foo'],
                'hyperparams': {'a': hpval, 'b': hpval, 'c': hpval}
            },
            'version': '1.0.0',
            'hyperparams': Hyperparams,
            'hyperparams_to_tune': ['a', 'b', 'c'],
            'installation': [{
                'type': PrimitiveInstallationType.PIP,
                'package_uri': 'https://foo.bar.com/blah.tar.gz'
             }],
            'schema': 'https://foo.com/bar/blah',
            'structural_type': float,
            'source': {
                'uris': ['http://foo.com'],
                'contact': 'mailto:author@example.com'
            },
        })

        js = metadata.metadata_to_json(self.pmd)
        self.pmd1 = metadata.json_to_metadata(js)

    # Just test that round trip worked at all
    def test_rt(self):
        self.assertTrue(isinstance(self.pmd1, PrimitiveMetadata))

    # Check that we got back all the keys we sent in at the top-level
    def test_keys(self):
        self.assertEqual(self.pmd.query().keys(), self.pmd1.query().keys())

    # Check the simple stuff (ints, strings, etc.)
    def test_simple(self):
        md = self.pmd.query()
        md1 = self.pmd1.query()
        for key in ['name', 'version', 'python_path']:
            self.assertEqual(md[key], md1[key])

    # Test enumerations
    def test_enum(self):
        md = self.pmd.query()
        md1 = self.pmd1.query()
        for key in ['algorithm_types', 'primitive_family']:
            self.assertEqual(md[key], md1[key])

    # Test hyperparams
    def test_hp(self):
        hp0 = self.pmd.query()['hyperparams']
        hp1 = self.pmd1.query()['hyperparams']
        self.assertTrue(issubclass(hp1, hp.Hyperparams))

        a = hp1.configuration['a']
        self.assertTrue(isinstance(a, hp.Bounded))
        self.assertEqual(a.lower, 0)
        self.assertEqual(a.upper, 1)
        self.assertEqual(a.get_default(), 0.5)

        b0 = hp0.configuration['b']
        b1 = hp1.configuration['b']
        self.assertEqual(b0.get_default(), b1.get_default())
        b0_foo_cfg = b0.choices['foo'].configuration
        b1_foo_cfg = b1.choices['foo'].configuration
        # Lame: we have to do a string comparison.
        self.assertEqual(str(b0_foo_cfg), str(b1_foo_cfg))

        # Check that infinity comes through ok.
        d = math.isinf(hp1.configuration['d'].get_default())

        # Check that unions come out ok
        e = hp1.configuration['e']
        values = list(e.traverse())
        self.assertTrue(isinstance(values[0], hp.Bounded))
        self.assertEqual(values[0].get_default(), 0.1)
        self.assertEqual(values[1].get_default(), 'xyz')

        # Check that Enumerations come out ok.
        f0 = hp0.configuration['f']
        f1 = hp1.configuration['f']
        # Looks like a tuple comes back as a list, probably because of JSON
        # encoding and decoding, not anything in d3m or here.
        self.assertEqual(list(f0.values), list(f1.values))

        # Check that Sets come out ok.
        g0 = hp0.configuration['g']
        g1 = hp1.configuration['g']
        self.assertEqual(str(g0), str(g1))

        # Check that Lists come out ok.
        h0 = hp0.configuration['h']
        h1 = hp1.configuration['h']
        self.assertEqual(str(h0), str(h1))


class TestDataMetadata(unittest.TestCase):
    def setUp(self):
        # Make up some data metadata. This is taken from d3m tests and must
        # match check_equal below.
        md = DataMetadata().update((ALL_ELEMENTS, 0), {'name': 'column1'})
        md = md.update((ALL_ELEMENTS, 1), {'name': 'column2'})
        md = md.update((10, 0), {'value': 'row10.0'})
        md = md.update((10, 1), {'value': 'row10.1'})

        # Make available to tests below.
        self.md = md

    def check_equal(self, md1):
        # From the d3m test cases.
        js = d3m.utils.to_json_structure(md1.to_internal_simple_structure())
        self.assertEqual(js, [{
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'column1'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'column2'},
        }, {
            'selector': [10, 0],
            'metadata': {'value': 'row10.0'},
        }, {
            'selector': [10, 1],
            'metadata': {'value': 'row10.1'},
        }])

    def test_simple(self):
        md1 = metadata.json_to_metadata(metadata.metadata_to_json(self.md))
        # Ensure same type as went in
        self.assertTrue(isinstance(md1, DataMetadata))
        self.check_equal(md1)

    # Check that we are able to pass a metadata object round trip via Pluros
    # functions.
    def test_input(self):
        md1 = pl.identity(self.md).get()
        self.assertTrue(isinstance(md1, DataMetadata))
        self.check_equal(md1)

    # Need more tests for DataMetadata - TODO.


if __name__ == '__main__':
    unittest.main()
