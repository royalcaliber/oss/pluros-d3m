#!/usr/bin/env python3

# Test generic proxy primitive for a bunch of primitives.

import unittest
import logging
import pickle

import d3m.index
import pluros as pl

from plurosd3m.primitives.proxy import proxy_primitive, is_proxy_primitive
import mkdataset


def get_default_hp(Prim):
    return Prim.metadata.query()['primitive_code'] \
        ['class_type_arguments']['Hyperparams'].defaults()  # noqa


# Test some basic functionality.
class TestSimple(unittest.TestCase):
    def test_is_proxy(self):
        Prim = proxy_primitive('d3m.primitives.classification.svc.SKlearn')
        self.assertTrue(is_proxy_primitive(Prim))

    # Check metadata property works as expected. Note that the metadata will
    # not necessarily match what we have locally if any.
    def test_metadata(self):
        Prim = proxy_primitive('d3m.primitives.classification.svc.SKlearn')
        name = Prim.metadata.query()['name']
        self.assertEqual(name, 'sklearn.svm.classes.SVC')
        # Check that metadata is cached
        self.assertTrue(Prim._metadata is not None)

    # Test that the proxy primitive is cached.
    def test_proxy_cache(self):
        Prim1 = proxy_primitive('d3m.primitives.classification.svc.SKlearn')
        Prim2 = proxy_primitive('d3m.primitives.classification.svc.SKlearn')
        self.assertIs(Prim1, Prim2)

    # Check that instances of the proxy class are picklable.
    def test_instance_pickle(self):
        Prim = proxy_primitive('d3m.primitives.classification.svc.SKlearn')
        prim = Prim(hyperparams=get_default_hp(Prim))
        s = pickle.dumps(prim)
        prim1 = pickle.loads(s)

    # Check that the proxy class itself is picklable.
    def test_class_pickle(self):
        Prim = proxy_primitive('d3m.primitives.classification.svc.SKlearn')
        s = pickle.dumps(Prim)
        Prim1 = pickle.loads(s)
        self.assertIs(Prim, Prim1)


# Test classification primitives
class TestClassifiers(unittest.TestCase):
    def setUp(self):
        self.dataset = mkdataset.binclass_dataset(
            1000, 'fff', singlePrecision=True)
        self.features = self.dataset['learningData'][['col0', 'col1', 'col2']]
        self.labels = self.dataset['learningData'][['class']]

    # Performs a computation using both the regular primitive and the proxy and
    # ensures results are statistically the same.
    def run_test_(self, Prim):
        prim = Prim(hyperparams=get_default_hp(Prim))
        prim.set_training_data(inputs=self.features, outputs=self.labels)
        prim.fit()
        pred = prim.produce(inputs=self.features)
        if pl.isValueWrapper(pred):
            pred = pl.get(pred)
        return pred

    def run_test(self, prim_path):
        try:
            LocalPrim = d3m.index.get_primitive(prim_path)
        except Exception as ex:
            logging.warning(
                'Could not load reference primitive, test is only partial')
            local_pred = None
        else:
            local_pred = self.run_test_(LocalPrim)
            if isinstance(local_pred.value, Exception):
                raise local_pred.value
        ProxyPrim = proxy_primitive(prim_path)
        proxy_pred = self.run_test_(ProxyPrim)
        # Check that proxy call succeeded.
        if isinstance(proxy_pred.value, Exception):
            raise proxy_pred.value

        # We can only check if we could run the reference. Otherwise the
        # test only ensures that the call completed.
        if local_pred is not None:
            # Usually the output is named 'class' but some primitives (e.g.
            # random.Test) return 'predictions' instead.
            local_pred.value.columns = local_pred.value.columns.astype(str)
            key = local_pred.value.columns[0]

            # Count number of discrepancies in classification
            ndiff = sum(local_pred.value[key] != proxy_pred.value[key])
            print(f'{ndiff} differed out of {len(local_pred.value)}')
            # Ensure less than 1%
            frac = ndiff / len(local_pred.value)
            self.assertLessEqual(frac, 0.01)

    def test_svc(self):
        self.run_test('d3m.primitives.classification.svc.SKlearn')

    def test_svc_pluros(self):
        self.run_test('d3m.primitives.classification.svc.Pluros')

    def test_rf(self):
        self.run_test('d3m.primitives.classification.random_forest.SKlearn')

    def test_adaboost(self):
        self.run_test('d3m.primitives.classification.ada_boost.SKlearn')

    def test_bagging(self):
        self.run_test('d3m.primitives.classification.bagging.SKlearn')

    def test_naive_bayes(self):
        self.run_test('d3m.primitives.classification'
                      '.bernoulli_naive_bayes.SKlearn')

#    def test_cover_tree(self):
#        self.run_test('d3m.primitives.classification.cover_tree.Fastlvm')

    def test_dt(self):
        self.run_test('d3m.primitives.classification.decision_tree.SKlearn')

    def test_dummy(self):
        self.run_test('d3m.primitives.classification.dummy.SKlearn')

    def test_extra_trees(self):
        self.run_test('d3m.primitives.classification.extra_trees.SKlearn')

    def test_gaussian_naive_bayes(self):
        self.run_test('d3m.primitives.classification'
                      '.gaussian_naive_bayes.SKlearn')

    def test_gradient_boosting(self):
        self.run_test('d3m.primitives.classification'
                      '.gradient_boosting.SKlearn')

    def test_k_neighbors(self):
        self.run_test('d3m.primitives.classification.k_neighbors.SKlearn')

    def test_lda(self):
        self.run_test('d3m.primitives.classification'
                      '.linear_discriminant_analysis.SKlearn')

    def test_linear_svc_distil(self):
        self.run_test('d3m.primitives.classification'
                      '.linear_svc.DistilRankedLinearSVC')

    def test_linear_svc(self):
        self.run_test('d3m.primitives.classification.linear_svc.SKlearn')

    def test_logistic_reg(self):
        self.run_test('d3m.primitives.classification'
                      '.logistic_regression.SKlearn')

    # This fails to match: need to investigate why.
    def test_mlp(self):
        self.run_test('d3m.primitives.classification.mlp.SKlearn')

    def test_nearest_centroid(self):
        self.run_test('d3m.primitives.classification'
                      '.nearest_centroid.SKlearn')

    def test_passive_aggressive(self):
        self.run_test('d3m.primitives.classification'
                      '.passive_aggressive.SKlearn')

    def test_qda(self):
        self.run_test('d3m.primitives.classification'
                      '.quadratic_discriminant_analysis.SKlearn')

    def test_random(self):
        self.run_test('d3m.primitives.classification.random_classifier.Test')

    def test_rf(self):
        self.run_test('d3m.primitives.classification.random_forest.SKlearn')

    def test_search(self):
        self.run_test('d3m.primitives.classification.search.Find_projections')

    def test_sgd(self):
        self.run_test('d3m.primitives.classification.sgd.SKlearn')


if __name__ == '__main__':
    unittest.main()
