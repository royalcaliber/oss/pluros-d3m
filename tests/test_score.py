#!/usr/bin/env python3

# Test d3m scoring function.

import unittest
import logging
import math

from sklearn import metrics
from sklearn.preprocessing import OneHotEncoder
import numpy as np
import pluros as pl


class TestScore(unittest.TestCase):
    # Generate data suitable for regression metrics.
    def gen_reg(self):
        y_true = np.random.normal(size=1000)
        y_pred = y_true + np.random.normal(scale=0.1, size=1000)
        return y_true, y_pred

    # Generate data suitable for classification. k is the number of classes.
    def gen_class(self, k=2):
        yc_true = np.random.randint(0, k, size=1000)
        rand = np.random.randint(0, k, size=1000)
        rtf = np.random.binomial(n=1, p=0.8, size=1000).astype('bool')
        yc_pred = np.select(
            [rtf, np.logical_not(rtf)], [yc_true, rand])
        return yc_true, yc_pred

    # Generate probabilities instead of classes from the true values.
    def gen_proba(self, y_true, k=2):
        # Generate random probabilities
        proba = np.random.normal(size=(len(y_true), k))
        # Set some percentage of them to reflect ground truth.
        idx = np.arange(0, len(y_true))
        np.random.shuffle(idx)
        idx = idx[:int(0.8 * len(y_true))]
        proba[idx, y_true[idx]] = 10
        # Row normalize
        proba /= proba.sum(axis=1, keepdims=True)
        return proba

    def test_accuracy(self):
        yc_true, yc_pred = self.gen_class(k=4)
        gold = metrics.accuracy_score(yc_true, yc_pred)
        test = pl.ml.d3m.score("accuracy", yc_true, yc_pred).get()
        self.assertAlmostEqual(test, gold)

    def test_precision(self):
        yc_true, yc_pred = self.gen_class(k=2)
        gold = metrics.precision_score(yc_true, yc_pred)
        test = pl.ml.d3m.score("precision", yc_true, yc_pred).get()
        self.assertAlmostEqual(test, gold)

    def test_recall(self):
        yc_true, yc_pred = self.gen_class(k=2)
        gold = metrics.recall_score(yc_true, yc_pred)
        test = pl.ml.d3m.score("recall", yc_true, yc_pred).get()
        self.assertAlmostEqual(test, gold)

    def test_f1(self):
        yc_true, yc_pred = self.gen_class(k=2)
        gold = metrics.f1_score(yc_true, yc_pred, pos_label=0)
        test = pl.ml.d3m.score("f1", yc_true, yc_pred, posLabel=0).get()
        self.assertAlmostEqual(test, gold)

    def test_f1_micro(self):
        yc_true, yc_pred = self.gen_class(k=4)
        gold = metrics.f1_score(yc_true, yc_pred, average='micro')
        test = pl.ml.d3m.score("f1_micro", yc_true, yc_pred).get()
        self.assertAlmostEqual(test, gold)

    def test_f1_macro(self):
        yc_true, yc_pred = self.gen_class(k=4)
        gold = metrics.f1_score(yc_true, yc_pred, average='macro')
        test = pl.ml.d3m.score("f1_macro", yc_true, yc_pred).get()
        self.assertAlmostEqual(test, gold)

    def test_roc_auc(self):
        yc_true, yc_pred = self.gen_class(k=4)
        proba = self.gen_proba(yc_pred, k=4)
        gold = metrics.roc_auc_score(yc_true, proba, multi_class='ovr')
        test = pl.ml.d3m.score("roc_auc", yc_true, proba).get()
        self.assertAlmostEqual(test, gold)

    def test_roc_auc_macro(self):
        yc_true, yc_pred = self.gen_class(k=4)
        proba = self.gen_proba(yc_pred, k=4)
        gold = metrics.roc_auc_score(yc_true, proba, multi_class='ovr',
                                     average='macro')
        test = pl.ml.d3m.score("roc_auc_macro", yc_true, proba).get()
        self.assertAlmostEqual(test, gold)

    # ROC_AUC_MICRO is not a valid combination in sklearn? Why is it
    # used in CMU TA2?

    def test_mean_squared_error(self):
        y_true, y_pred = self.gen_reg()
        gold = metrics.mean_squared_error(y_true, y_pred)
        test = pl.ml.d3m.score("mean_squared_error", y_true, y_pred).get()
        self.assertAlmostEqual(gold, test)

    def test_root_mean_squared_error(self):
        y_true, y_pred = self.gen_reg()
        gold = math.sqrt(metrics.mean_squared_error(y_true, y_pred))
        test = pl.ml.d3m.score("root_mean_squared_error", y_true, y_pred).get()
        self.assertAlmostEqual(gold, test)

    def test_mean_absolute_error(self):
        y_true, y_pred = self.gen_reg()
        gold = metrics.mean_absolute_error(y_true, y_pred)
        test = pl.ml.d3m.score("mean_absolute_error", y_true, y_pred).get()
        self.assertAlmostEqual(gold, test)

    def test_r_squared(self):
        y_true, y_pred = self.gen_reg()
        gold = metrics.r2_score(y_true, y_pred)
        test = pl.ml.d3m.score("r_squared", y_true, y_pred).get()
        self.assertAlmostEqual(gold, test)

    def test_normalized_mutual_info_score(self):
        yc_true, yc_pred = self.gen_class(k=4)
        gold = metrics.normalized_mutual_info_score(yc_true, yc_pred)
        test = pl.ml.d3m.score("normalized_mutual_information", yc_true,
                               yc_pred).get()
        self.assertAlmostEqual(gold, test)

    def test_jaccard_similarity(self):
        yc_true, yc_pred = self.gen_class(k=4)
        gold = metrics.jaccard_similarity_score(yc_true, yc_pred)
        test = pl.ml.d3m.score("jaccard_similarity_score", yc_true,
                               yc_pred).get()
        self.assertAlmostEqual(gold, test)


if __name__ == '__main__':
    unittest.main()
