#!/usr/bin/env python3

# Test SKDecisionTreeClassifier and SKDecisionTreeRegressor

import numpy as np
import sklearn.metrics
import unittest

from d3m.primitives.classification.decision_tree import SKlearn as SKDTC
from d3m.primitives.regression.decision_tree import SKlearn as SKDTR

from plurosd3m.tools import optimize_pipeline
import mkdataset
import mkpipeline
from miscutil import *


class TestDecisionTree(unittest.TestCase):
    def _run_test(self, type, rtol=0.05, **hp):
        np.random.seed(12341)
        if type == 'c':
            prim = SKDTC
            metric = sklearn.metrics.f1_score
        else:
            prim = SKDTR
            metric = sklearn.metrics.mean_squared_error
        dataset = mkdataset.binclass_dataset(1000, 'fff',
                                             singlePrecision=True)
        truth = dataset['learningData']['class']
        ppln = mkpipeline.gen_cr_pipeline(
            prim, scaled=False, type='c')
        if hp:
            set_hp(ppln, **hp)
        score = run_ppln(ppln, dataset, truth, metric)
        assert optimize_pipeline(ppln)
        score_opt = run_ppln(ppln, dataset, truth, metric)
        print('score=', score, 'score_opt=', score_opt)
        if type == 'c':
            assert score_opt >= score * (1 - rtol)
        else:
            assert score_opt <= (score + 1e-6) * (1 + rtol)

    def test_defaults(self):
        self._run_test(type='c')

    def test_defaults_r(self):
        self._run_test(type='r')


if __name__ == '__main__':
    unittest.main()
