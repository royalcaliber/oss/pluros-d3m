#!/usr/bin/env python3

# Test the accelerated GradientBoosting* primitives in simple pipelines
# executed via the reference runtime.

import unittest
import mkpipeline
import mkdataset
import d3m.runtime
import d3m.cli
from d3m.metadata.base import ArgumentType
from d3m.metadata.problem import PerformanceMetric
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from d3m.primitives.classification.gradient_boosting import SKlearn as SKGBC
from d3m.primitives.regression.gradient_boosting import SKlearn as SKGBR
from plurosd3m.tools import optimize_pipeline
import numpy as np
import sklearn
import sklearn.metrics as skmetrics
import copy


def getId(p):
    return p.metadata.query()['id']


# Factor with test_sk_svm. TODO.

class TestGBM(unittest.TestCase):
    """Tests Pluros accelerated GBM primitive"""

    # Generates a scaled and an unscaled pipeline using SVC, along with a few
    # simple tables to serve as inputs.
    #
    # We convert the pipelines to JSON so that we can create independent
    # instances of them for each test.
    @classmethod
    def setUpClass(cls):
        np.random.seed(12341)
        cls.prims = [getId(SKGBC), getId(SKGBR)]
        cls.dataset = mkdataset.binclass_dataset(1000, 'fff',
                                                 singlePrecision=True)
        cls.truth = cls.dataset['learningData']['class']
        cls.gbc_scaled = mkpipeline.gen_cr_pipeline(
            SKGBC, scaled=True, type='c').to_json_structure()
        cls.gbr_scaled = mkpipeline.gen_cr_pipeline(
            SKGBR, scaled=True, type='r').to_json_structure()
        cls.gbc_unscaled = mkpipeline.gen_cr_pipeline(
            SKGBC, scaled=False, type='c').to_json_structure()
        cls.gbr_unscaled = mkpipeline.gen_cr_pipeline(
            SKGBR, scaled=False, type='r').to_json_structure()

    # Set a hyper parameter on the GBX primitive in a pipeline.
    def _set_hp(self, ppln, **hp):
        for step in ppln.steps:
            if isinstance(step, PrimitiveStep):
                id = step.primitive.metadata.query()['id']
                if id in self.prims:
                    for k, v in hp.items():
                        step.add_hyperparameter(
                            name=k, data=v, argument_type=ArgumentType.VALUE)

    def _get_ppln(self, pplnjs):
        return Pipeline.from_json_structure(pplnjs)

    def _run_ppln(self, ppln, type='c'):
        r = d3m.runtime.Runtime(pipeline=ppln, context='TESTING')
        result = r.fit([self.dataset])
        if result.has_error():
            raise result.error
        if type == 'c':
            return sklearn.metrics.f1_score(
                self.truth, result.values['outputs.0'])
        else:
            return sklearn.metrics.mean_squared_error(
                self.truth, result.values['outputs.0'])

    def _run_test(self, scaled, type='c', rtol=0.05, **hp):
        if type == 'c':
            p = self._get_ppln(
                self.gbc_scaled if scaled else self.gbc_unscaled)
        else:
            p = self._get_ppln(
                self.gbr_scaled if scaled else self.gbr_unscaled)
        if hp:
            self._set_hp(p, **hp)
        score = self._run_ppln(p, type=type)
        assert optimize_pipeline(p)
        score_opt = self._run_ppln(p, type=type)
        print('score=', score, 'score_opt=', score_opt)
        if type == 'c':
            assert score_opt >= score * (1 - rtol)
        else:
            assert score_opt <= score * (1 + rtol)

    def test_scaled(self):
        self._run_test(scaled=True)

    def test_scaled_r(self):
        self._run_test(scaled=True, type='r')

    def test_unscaled(self):
        self._run_test(scaled=False)

    def test_unscaled_r(self):
        self._run_test(scaled=False, type='r')


if __name__ == '__main__':
    unittest.main()
