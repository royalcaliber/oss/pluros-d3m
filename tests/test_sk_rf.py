#!/usr/bin/env python3

# Test SKRandomForestClassifier and SKRandomForestRegressor

import numpy as np
import sklearn.metrics
import unittest
from collections import OrderedDict
import logging
import pickle

from d3m.primitives.classification.random_forest import SKlearn as SKRFC
from d3m.primitives.regression.random_forest import SKlearn as SKRFR

from d3m.primitives.classification.random_forest import Pluros as PlurosRFC
from d3m.primitives.regression.random_forest import Pluros as PlurosRFR
from plurosd3m.primitives.sklearn.SKRandomForestClassifier \
    import SKRandomForestClassifier as OldRFC
from plurosd3m.primitives.sklearn.SKRandomForestRegressor \
    import SKRandomForestRegressor as OldRFR
from plurosd3m.primitives.proxy import proxy_primitive
from plurosd3m.tools import optimize_pipeline
import mkdataset
import mkpipeline
from miscutil import *


ProxyRFC = proxy_primitive(
    "d3m.primitives.classification.random_forest.SKlearn")

ProxyRFR = proxy_primitive(
    "d3m.primitives.regression.random_forest.SKlearn")


class TestRFNew(unittest.TestCase, TestBase):
    @classmethod
    def setUpClass(cls):
        np.random.seed(1234)
        cls.dataset = mkdataset.binclass_dataset(100, 'fff',
                                                 singlePrecision=True)
        cls.truth = cls.dataset['learningData']['class']
        cls.rtruth = cls.truth.astype('f4')

    def test_scaled(self):
        pplns = self.mk_pplns(
            scaled=True,
            type='c',
            prims=OrderedDict([
                (SKRFC, {}),
                (PlurosRFC, {}),
                (OldRFC, {}),
                (ProxyRFC, {}),
            ])
        )
        self.run_test(pplns, self.dataset, self.truth, "c")

    def test_scaled_r(self):
        pplns = self.mk_pplns(
            scaled=True,
            type='r',
            prims=OrderedDict([
                (SKRFR, {}),
                (PlurosRFR, {}),
                (OldRFR, {}),
                (ProxyRFR, {}),
            ])
        )
        self.run_test(pplns, self.dataset, self.rtruth, "r")

    # Test that data comes back with the same dtype.
    def test_output_dtype(self):
        hp = get_hyperparams(PlurosRFC, max_depth=3)
        prim = PlurosRFC(hyperparams=hp)
        x = self.dataset['learningData'].drop('class', axis=1)
        # Encode as decimal strings.
        y = self.dataset['learningData'][['class']].astype('int').astype('str')
        prim.set_training_data(inputs=x, outputs=y)
        prim.fit()
        ypred = prim.produce(inputs=x).value.get()
        self.assertEqual(y['class'].dtype, ypred['class'].dtype)

    # Test that we can pickle/unpickle the new RF primitive.
    def test_pickle_unfitted(self):
        hp = get_hyperparams(PlurosRFC, max_depth=3)
        prim = PlurosRFC(hyperparams=hp)
        s = pickle.dumps(prim)
        prim1 = pickle.loads(s)
        self.assertEqual(prim1.hyperparams['max_depth'], 3)

    def test_pickle_fitted(self):
        hp = get_hyperparams(PlurosRFC)
        prim = PlurosRFC(hyperparams=hp)
        x = self.dataset['learningData'].drop('class', axis=1)
        y = self.dataset['learningData'][['class']]
        prim.set_training_data(inputs=x, outputs=y)
        prim.fit()
        s = pickle.dumps(prim)
        prim1 = pickle.loads(s)
        yout = prim.produce(inputs=x).get()
        yout1 = prim1.produce(inputs=x).get()
        self.assertTrue(np.all(yout.value == yout1.value))


class TestRF(unittest.TestCase):
    def _run_test(self, type, rtol=0.05, **hp):
        np.random.seed(12341)
        if type == 'c':
            prim = SKRFC
            metric = sklearn.metrics.f1_score
        else:
            prim = SKRFR
            metric = sklearn.metrics.mean_squared_error
        dataset = mkdataset.binclass_dataset(1000, 'fff',
                                             singlePrecision=True)
        truth = dataset['learningData']['class']
        ppln = mkpipeline.gen_cr_pipeline(
            prim, scaled=False, type='c')
        if hp:
            set_hp(ppln, **hp)
        score = run_ppln(ppln, dataset, truth, metric)
        assert optimize_pipeline(ppln)
        score_opt = run_ppln(ppln, dataset, truth, metric)
        print('score=', score, 'score_opt=', score_opt)
        if type == 'c':
            assert score_opt >= score * (1 - rtol)
        else:
            assert score_opt <= score * (1 + rtol)

    def test_defaults(self):
        self._run_test(type='c')

    def test_defaults_r(self):
        self._run_test(type='r')


if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)
    unittest.main()
