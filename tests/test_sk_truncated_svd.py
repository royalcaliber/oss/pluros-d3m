#!/usr/bin/env python3

# Test accelerated truncated SVD using the reference runtime.

import unittest
import mkpipeline
import mkdataset
import d3m.runtime
import d3m.cli
from d3m.metadata.base import ArgumentType
from d3m.metadata.problem import PerformanceMetric
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from d3m.primitives.data_preprocessing.truncated_svd import SKlearn as SKTSVD
from plurosd3m.primitives.sklearn.SKTruncatedSVD import SKTruncatedSVD as OldSKTSVD
from d3m.primitives.feature_extraction.truncated_svd import Pluros as PlurosTSVD
from plurosd3m.tools import optimize_pipeline
from plurosd3m.primitives.proxy import proxy_primitive
import numpy as np
import sklearn
import sklearn.metrics as skmetrics
import copy
from collections import OrderedDict
from d3m.container.pandas import DataFrame

from scipy.stats import ortho_group
import miscutil
import pluros as pl


ProxyTSVD = proxy_primitive(
    "d3m.primitives.feature_extraction.truncated_svd.SKlearn")


class TestTruncatedSVD(unittest.TestCase):

    # Cut-and-pasted from pluros-python-client/tests/test_tsvd.py
    #
    # Generates a random matrix with k distinct singular values.
    def genmat(self, m, n, k, dt):
        U = ortho_group.rvs(m)
        Vh = ortho_group.rvs(n)
        s = np.zeros(min(m, n))
        s[:k] = 2 + np.arange(k)
        s[k:] = np.random.uniform(0, 1, size=min(m, n) - k)
        S = np.zeros((m, n))
        np.fill_diagonal(S, s)
        return (U @ S @ Vh).astype(dt)

    # Run pipeline p on dataset ds
    def runit(self, p, ds):
        r = d3m.runtime.Runtime(pipeline=p, context='TESTING')
        result = r.fit([ds])
        if result.has_error():
            raise result.error
        result = r.produce([ds])
        if result.has_error():
            raise result.error
        return result.values['outputs.0']

    def test_new(self):
        nRows = 100
        nCols = 50
        k = 5
        x = self.genmat(nRows, nCols, k, np.dtype('f4'))
        ds = mkdataset.df_to_d3m({'col%d' % i: x[:, i] for i in range(nCols)})
        prims = OrderedDict([
            (SKTSVD, {'n_components': k}),
            (PlurosTSVD, {'k': k}),
            (OldSKTSVD, {'n_components': k}),
            (ProxyTSVD, {'n_components': k})
        ])
        pplns = [
            mkpipeline.gen_tsvd_pipeline(prim, hp)
                for prim, hp in prims.items()
        ]
        # This just ensures everything runs. We should check that cluster
        # assignments are approximately the same. TODO.
        out = pl.get(*(self.runit(p, ds) for p in pplns))


    def test_dense(self):
        # Get something with 5 distinct largest singular values.
        nRows = 100
        nCols = 50
        k = 5
        x = self.genmat(nRows, nCols, k, np.dtype('f4'))
        # convert to a table
        tab = {'col%d' % i: x[:, i] for i in range(nCols)}
        # convert to a D3M dataset.
        ds = mkdataset.df_to_d3m(tab)
        p = mkpipeline.gen_tsvd_pipeline(SKTSVD, {'n_components': k})

        # Run unoptimized
        ref = self.runit(p, ds)

        # Run optimized.
        assert optimize_pipeline(p)
        acc = self.runit(p, ds)

        # Compare. This comparison is not quit right due to the abs...
        assert np.allclose(np.abs(ref), np.abs(acc), atol=1e-2, rtol=1e-2)

    def test_sparse(self):
        pass


if __name__ == '__main__':
    unittest.main()
